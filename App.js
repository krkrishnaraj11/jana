import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Header} from 'native-base';

export default class App extends Component {
  render() {

    return (
      <View>
            <StatusBar 
            backgroundColor={'transparent'}/>
        <Header>
        <LinearGradient
         start={{x: 0, y: 0}} end={{x: 1, y: 0}} 
      colors={['#8D218B', '#DA215B', '#F0522D']}
      style={{height:55, width:450}}
    >
    <View>
    <Text style={styles.welcome}>Welcome to React Native!</Text>

      </View></LinearGradient>
        </Header>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
