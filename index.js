/** @format */

import {AppRegistry} from 'react-native';
import App from './src/App';
import Root from './src/RootContainer';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Root);
