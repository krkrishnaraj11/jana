import React, { Component } from 'react';
import {  StatusBar, ActivityIndicator,Dimensions, AsyncStorage, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import moment from 'moment';
import axios from 'axios';
import { API } from '../config/Api';
import LinearGradient from 'react-native-linear-gradient';
import { Header } from 'native-base';
var { height, width } = Dimensions.get('window');
import { styles } from './styles/ActivityContainerStyles';
import StepProgress from '../components/StepProgess';

export default class Activity extends Component {
    constructor() {
        super();
        this.state = {
            token: '',
            myKey: '',
            activityList: []
        }
    }

    fetchActivityList() {
        axios({
            url: API.activityList,
            method: 'get',
            headers: {
                Authorization: `Bearer ${this.state.token}`
            }
        })
            .then((res) => {
                this.setState({
                    activityList: res.data
                })
            })
            .catch((error) => {
                console.log(error);
            });

    }

    async getItem(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            this.setState({ myKey: JSON.parse(value) });
            if (this.state.myKey.response_code == "success") {
                this.setState({
                    token: this.state.myKey.access_token
                })
                this.fetchActivityList();
            }
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
        return Promise.resolve(this.state.token)
    }

    componentWillMount() {
        console.log("AWAIT:", this.getItem('loginResponse'));
        // this.fetchActivityList();
    }

    renderActivityList(data) {
       var date = moment(data.item.activity_datetime).format('D MMM YYYY, h:mm:ss a');
       console.log(data.item)
        return (
            <View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: height / 40 }}>
                    <View style={{ width: width / 1.7, marginLeft: width / 20 }}>
                        <Text style={{
                            fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                height / 42, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                        }}>{'Loan for '+data.item.client_first_name + ' ' + data.item.client_second_name + ' ' + data.item.client_third_name + ' ' + data.item.client_last_name}</Text>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>{data.item.activity_text + ' by ' + data.item.authority_name }</Text>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>{date}</Text>
                    </View>

                    <View style={{ width: width / 4, marginTop: height / 100, alignItems: 'center', marginRight: 30, marginLeft: 10 }}>
                        <StepProgress step={data.item.step} total={5}/>
                    </View>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'center', marginTop: height / 50 }} />
            </View>
        )
    }
    render() {
        return (
            <View style={{ backgroundColor: '#F8FCFF' }}>
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                    <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <View>
                            <Text style={styles.homeTitle}>Activity</Text>

                        </View>
                    </LinearGradient>
                </Header>

                <View style={{ height: height, marginTop: 20, width: width, backgroundColor: '#fff' }}>
                {(this.state.activityList.length == 0 )
                    ? <ActivityIndicator size="large" color="#8D218B" style={{ marginTop: height/3}}/>
                    : 
                    <FlatList
                        data={this.state.activityList.data}
                        renderItem={(item) => this.renderActivityList(item)}
                    />
                }
                    
                </View>
            </View>
        );
    }
}
