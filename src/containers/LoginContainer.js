import React, { Component } from 'react';
import { AsyncStorage, StatusBar, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import FloatingLabelInput from '../components/FloatingLabelInput';
import axios from 'axios';
import { API } from '../config/Api';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
var { height, width } = Dimensions.get('window');
export default class Root extends Component {
    constructor() {
        super();
        this.state = {
            username: 'admin1@jana.com',
            password: 'test@123',
            passSecure: true,
            loginResponse: {},
            myKey: ''
        };
    }

    getUsername(text) {
        this.setState({
            username: text
        });
    }

    getPassword(text) {
        this.setState({
            password: text
        });
    }

    viewPassword() {
        this.setState({
            passSecure: !this.state.passSecure
        })
    }


    async getItem(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            this.setState({ myKey: JSON.parse(value) });
            console.log("$$", this.state.myKey)
            if (this.state.myKey.response_code == "success") {
                this.setState({
                    registered: true
                })
            }
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
        return Promise.resolve(this.state.registered)
    }



    async storeItem(key, item) {
        try {
            //we want to wait for the Promise returned by AsyncStorage.setItem()
            //to be resolved to the actual value before returning the value
            var jsonOfItem = await AsyncStorage.setItem(key, item);
            return jsonOfItem;
        } catch (error) {
            console.log(error.message);
        }
    }

    login = () => {
        const fd = new FormData();
        fd.append('email', this.state.username);
        fd.append('password', this.state.password);

        axios(API.login, {
            method: 'post',
            data: fd,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        })
            .then((res) => {
                console.log(res.data);
                this.setState({
                    loginResponse: res.data
                })
                login_token = res.data.access_token;
                this.storeItem("loginResponse", JSON.stringify(res.data));
                console.log("AWAIT:", this.getItem('loginResponse'))

                this.props.navigation.navigate('Home');

            })
            .catch((error) => {
                console.log(error);
            });

        console.log(this.state.loginResponse);
    }

    render() {
        return (
            <KeyboardAwareScrollView enableOnAndroid style={styles.container}>
                <StatusBar
                    backgroundColor={'transparent'} />
                <Image
                    source={require('../assets/images/login/logo.png')}
                    resizeMode={'contain'}
                    style={{ height: height / 4, width: width / 4, alignSelf: "center", marginTop: height / 11 }}
                />
                <Text style={styles.signIn}>SIGN IN</Text>
                <View style={{ borderWidth: 0, width: width / 1.3, alignSelf: "center" }}>
                    <FloatingLabelInput
                        label="Username"
                        width={width / 1.4}
                        value={this.state.username}
                        onChangeText={(text) => this.getUsername(text)}
                        onSubmitEditing={() => this.customInput2.refs.innerTextInput2.focus()}
                        style={styles.usernameStyle} />
                    <View style={{ flexDirection: "row" }}>
                        <FloatingLabelInput
                            label="Password"
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            value={this.state.password}
                            width={width / 1.4}
                            secureTextEntry={this.state.passSecure}
                            onChangeText={(text) => this.getPassword(text)}
                            style={styles.passwordStyle} />

                        <TouchableOpacity onPress={() => this.viewPassword()}>
                            <Image
                                source={(this.state.passSecure) ? require('../assets/images/login/eye-open.png') : require('../assets/images/login/eye-closed.png')}
                                resizeMode={"contain"}
                                style={{ height: height / 13, width: width / 13, right: width / 13, top: height / 120 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity>
                        <Text style={{ fontFamily: "Lato-Regular", textAlign: "right", width: width / 1.4, color: "#D72163" }}>Forgot Password?</Text>
                    </TouchableOpacity>

                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                        style={{ height: height / 17, width: width / 1.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 16, borderRadius: 20 }}
                    >
                        <TouchableOpacity style={{ height: height / 18, width: width / 1.34, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.login()}>
                            <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>SIGN IN</Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </View>
                <Text style={{ fontSize: 12, fontFamily: "PingFangSC-Regular", color: "rgba(0,0,0,0.32)", alignSelf: "center", marginTop: height / 8 }}>© JANA2018</Text>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    signIn: {
        color: "#D72163",
        alignSelf: "center",
        fontSize: 14,
        marginTop: height / 40,
        marginBottom: 60,
    },
    usernameStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    passwordStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
        borderWidth: 1
    }

});
