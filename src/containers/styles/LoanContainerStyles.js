import {StyleSheet, Dimensions, Platform, StatusBar} from 'react-native';
var {height, width} = Dimensions.get('window');

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/4,
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height/20,
    },
    gradientStyle:{
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 30 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height/12 : height/8,
        paddingTop: height/70
    },
    headerStyle:{
        marginBottom: (Platform.OS == 'ios') ? height/30 : height/20,
        // height: height/10
    },
    statusTitle:{
        fontFamily:'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign:'left',
        top: (Platform.OS == 'android') ? height/55 : 1,
        marginLeft: height/40  
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/4,
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        left: width/50,
        alignSelf: 'center'
    },
    searchIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        alignSelf: 'center'
    },
    plusIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 40,
        alignSelf: 'center'
    },
});
