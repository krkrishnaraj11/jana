import React, { Component } from 'react';
import { Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, Platform, StatusBar } from 'react-native';
import { Header } from 'native-base';
import StepProgressLoan from '../components/StepProgressLoan';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');

export default class CLIStepIV extends Component {
    constructor() {
        super();
        this.state = {
            IBAN: '',
            IBANConfirm: '0',
            fileAttached: true,
            clientBank: ''
        }
    }

    getInstallAmount(text) {
        this.setState({
            installAmount: text
        });
    }

    getLoanCycle(text) {
        this.setState({
            loanCycle: text
        });
    }

    render() {
        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                    <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <View style={{flexDirection: 'row', right:20, top: (Platform.OS == 'ios') ? 10 : 0}}>
                        <TouchableOpacity style={{marginLeft: height / 50, marginTop:width/12}} onPress={()=> this.props.navigation.goBack()}>
                            <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        </TouchableOpacity>
                            <Text style={styles.homeTitle}>Create New Loan</Text>
                        </View>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center',  flex:1, paddingLeft: width/20}}>
                    <StepProgressLoan step={4} total={10} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', justifyContent: 'space-evenly' }}>CLIENT BANK DETAILS</Text>

                    <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ width: width / 1.1, marginTop: height / 100}}>
                        <View style={{ flexDirection: "row", width: width }}>
                            <FloatingLabelInput
                                label="IBAN"
                                value={this.state.IBAN}
                                width={width / 1.2}
                            onChangeText={() => this.getIBAN()}
                            />

                            <TouchableOpacity>
                                <Image
                                    source={require('../assets/images/CLI3/attachment.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 15, right: width / 15, top: height / 50 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <FloatingLabelInput
                                label="Confirm IBAN"
                                // value={this.state.IBANConfirm}
                                width={width / 1.2}
                            // onChangeText={() => this.getIBANConfirm()}
                            />
                        <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 50, color: 'rgba(0,0,0,0.32)' }}>File Attached</Text>
                        <View style={{ alignItems: 'center', height: height / 7, backgroundColor: '#FAFAFA', justifyContent: 'center' }}>
                            <Image
                                source={require('../assets/images/CLI3/file-attach.png')}
                                resizeMode={"contain"}
                                style={{ height: height / 10, width: height / 10 }}
                            />
                            <TouchableOpacity style={{ position: 'absolute', right: width / 3, top: height / 70 }}>
                                <Image
                                    source={require('../assets/images/CLI3/error.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 40, width: height / 40, alignSelf: 'center' }}
                                />
                            </TouchableOpacity>
                        </View>
                        <FloatingLabelInput
                            label="Client Bank"
                            value={this.state.clientBank}
                            returnKeyType={"done"}
                            style={{marginLeft: width/2}}
                            autoFocus={false}
                            width={width / 1.2}
                            onChangeText={(text) => this.getGooglePlus                                                                                                                                     (text)}
                        />
                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                        style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height/20 }}>
                        <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('CLIStepV')}>
                            <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    </KeyboardAwareScrollView>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/8,
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height/20,
    },
    gradientStyle:{
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 40 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height/14 : height/7,
        paddingTop: height/70
    },
    headerStyle:{
        marginBottom: (Platform.OS == 'ios') ? height/50 : height/20,
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});

