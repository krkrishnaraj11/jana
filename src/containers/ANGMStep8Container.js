import React, { Component } from 'react';
import { StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header, Card } from 'native-base';
import StepProgressGroupCreation from '../components/StepProgessGroupCreation';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');


export default class ANGMStepVIII extends Component {
    constructor(){
        super();
        this.state = {

        }
    }

    render(){
        let documentData = [{
            value: 'ID',
        }, {
            value: 'Salary Verification',
        }, {
            value: 'Bank Account Card',
        }];
        return(
        <View style={styles.container}>
            <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center',  flex:0}}>
                    <StepProgressGroupCreation step={8} total={8} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)'}}>DOCUMENTS</Text>
                </View>
            <KeyboardAwareScrollView style={styles.container}>
                

                <View style={{width: width/1.2, alignSelf:'center'}}>
                    <Dropdown
                        label={"Select Uploaded Documents"}
                        data ={documentData}
                    />
                </View>
                <LinearGradient
                    colors={['#8D218B', '#DA215B', '#F0522D']}
                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                    style={{ height: height / 17, width: width / 2.1, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height/40 }}>
                    <TouchableOpacity style={{ height: height / 18, width: width / 2.13, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} >
                        <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163"}}>ATTACH DOCUMENTS</Text>
                    </TouchableOpacity>
                </LinearGradient>

                <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 50, color: 'rgba(0,0,0,0.54)', alignSelf:'center'}}>DOCUMENTS</Text>
                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/15, width: height/15}}/>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center'}}>ID</Text>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/15, width: height/15}}/>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center'}}>Salary Verification</Text>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/15, width: height/15}}/>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center'}}>Bank Account Card</Text>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <LinearGradient
                    colors={['#8D218B', '#DA215B', '#F0522D']}
                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                    style={{ height: height / 17, width: width / 2.1, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height/50 }}>
                    <TouchableOpacity style={{ height: height / 18, width: width / 2.13, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('ANGMemberList')}>
                        <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>ADD MEMBER</Text>
                    </TouchableOpacity>
                </LinearGradient>

                <TouchableOpacity>
                    <Card style={{flexDirection: 'row', marginTop: height/100, justifyContent: 'space-between', width: width/1.03, alignSelf:'center', height: height/12,borderRadius: width/50}}>
                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']} style={{ height: height/12,width:width/5, borderTopLeftRadius: width/50, borderBottomLeftRadius:width/50, justifyContent:'center'}}>
                        <Image source={require('../assets/images/CLI10/digital_signature.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                        </LinearGradient>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'center', width: width/1.5, alignSelf:'center'}}>Digital Signature</Text>
                        <Image source={require('../assets/images/CLI10/check-circle-active.png')} style={{ height: height/23, width: height/23, alignSelf: 'center', marginRight: width/20}}/>
                    </Card>
                </TouchableOpacity>

                <TouchableOpacity>
                    <Card style={{flexDirection: 'row', marginTop: height/100, justifyContent: 'space-between', width: width/1.03, alignSelf:'center', height: height/12,borderRadius: width/50,marginBottom: height/30 }}>
                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']} style={{ height: height/12,width:width/5, borderTopLeftRadius: width/50, borderBottomLeftRadius:width/50, justifyContent:'center'}}>
                        <Image source={require('../assets/images/CLI10/finger_print.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                        </LinearGradient>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'center', width: width/1.5, alignSelf:'center'}}>Capture Fingerprint</Text>
                        <Image source={require('../assets/images/CLI10/check-circle-inactive.png')} style={{ height: height/23, width: height/23, alignSelf: 'center', marginRight: width/20}}/>
                    </Card>
                </TouchableOpacity>

            </KeyboardAwareScrollView>
            </View>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        // alignItems:'center'
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});