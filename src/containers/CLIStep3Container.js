 import React, { Component } from 'react';
import { Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Platform, StatusBar } from 'react-native';
import { Header } from 'native-base';
import moment from 'moment';
import {API} from '../config/Api';
import axios from 'axios';
import StepProgressLoan from '../components/StepProgressLoan';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import FloatingLabelInput from '../components/FloatingLabelInput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { RNS3 } from 'react-native-aws3';
var { height, width } = Dimensions.get('window');


export default class CLIStepIII extends Component {
    constructor() {
        super();
        this.state = {
            ID: '',
            firstName: '',
            secondName: '',
            thirdName: '',
            LastName: '',
            expiryDate: '',
            dateOfBirth: '',
            issueDate: '',
            datePickerVisible1: false,
            datePickerVisible2: false,
            datePickerVisible3: false,
            age: '',
            contactNumber: '',
            email: '',
            data: [],
            nationality:'',
            qualifcation: '',
            isWorking: false,
            placeOfWork: '',
            designation: '',
            monthlySalary: '',
            facebook: '',
            linkedIn: '',
            twitter: '',
            googleplus: '',
            fileUri: '',
            fileType: '',
            fileName: '',
            fileSize: '',
            fileAttached: false
        }
    }

    handleChange() {
        DocumentPicker.show(
            {
            filetype: [DocumentPickerUtil.images()],
            //All type of Files DocumentPickerUtil.allFiles()
            //Only PDF DocumentPickerUtil.pdf()
            //Audio DocumentPickerUtil.audio()
            //Plain Text DocumentPickerUtil.plainText()
            },
            (error, res) => {
            this.setState({ fileUri: res.uri });
            this.setState({ fileType: res.type });
            this.setState({ fileName: res.fileName });
            this.setState({ fileSize: res.fileSize });
            this.setState({ fileAttached: true });
            console.log('res : ' + JSON.stringify(res));
            console.log('URI : ' + res.uri);
            console.log('Type : ' + res.type);
            console.log('File Name : ' + res.fileName);
            console.log('File Size : ' + res.fileSize);
            }
        );


        const file = {
            // `uri` can also be a file system path (i.e. file://)
            uri: this.state.fileUri,
            name: this.state.fileName,
            type: "image/png"
          }
          
        const options = {
        keyPrefix: "uploads/",
        bucket: "storage-vro",
        region: "us-east-1",
        accessKey: "AUQFNaJMteqWh26rEiBKYi4SC7wn9DJm+CA2ihLK",
        secretKey: "AKIAJ3YPEZX2VVKJBYAA",
        successActionStatus: 201
        }

        RNS3.put(file, options).then(response => {
            if (response.status !== 201)
              throw new Error("Failed to upload image to S3");
            console.log(response.body);
            /**
             * {
             *   postResponse: {
             *     bucket: "your-bucket",
             *     etag : "9f620878e06d28774406017480a59fd4",
             *     key: "uploads/image.png",
             *     location: "https://your-bucket.s3.amazonaws.com/uploads%2Fimage.png"
             *   }
             * }
             */
          });
    }

    getID(text) {
        this.setState({
            ID: text
        })
    }

    getFirstName(text) {
        this.setState({
            firstName: text
        });
    }

    getSecondName(text) {
        this.setState({
            secondName: text
        });
    }

    getThirdName(text) {
        this.setState({
            thirdName: text
        });
    }

    getLastName(text) {
        this.setState({
            LastName: text
        });
    }

    getExpiryDate(text) {
        this.setState({
            expiryDate: text
        });
    }

    getIssueDate(text) {
        this.setState({
            issueDate: text
        });
    }

    getDateOfBirth(text) {
        this.setState({
            dateOfBirth: text
        });
    }

    getAge(text) {
        this.setState({ age: text })
    }
    
    getEmail(text){
        this.setState({ email: text })
    }

    getContactNumber(text) {
        this.setState({ contactNumber: text })
    }

    getPlaceofWork(text) {
        this.setState({ placeOfWork: text })
    }

    getDesignation(text) {
        this.setState({ designation: text })
    }

    getMonthlySalary(text) {
        this.setState({ monthlySalary: text })
    }
    getFacebook(text) {
        this.setState({ facebook: text })
    }

    getLinkedIn(text) {
        this.setState({ linkedIn: text })
    }

    getTwitter(text) {
        this.setState({ twitter: text })
    }

    getGooglePlus(text) {
        this.setState({ googleplus: text })
    }

    renderDatePicker1() {
        this.setState({ datePickerVisible1: !this.state.datePickerVisible1 });
    }

    renderDatePicker2() {
        this.setState({ datePickerVisible2: !this.state.datePickerVisible2 });

    }

    renderDatePicker3() {
        this.setState({ datePickerVisible3: !this.state.datePickerVisible3 });

    }

    handleDatePicked = (dob) => {
        this.setState({ dateOfBirth: moment(dob).format('DD/MM/YYYY')});
        console.log("export",this.state.dateOfBirth);

        this.renderDatePicker1();
    };

    handleDatePickedExpiry = (date) => {
        this.setState({ expiryDate: moment(date).format('DD/MM/YYYY')});
        console.log("export",this.state.expiryDate);
        this.renderDatePicker2();
    };

    handleDatePickedIssue = (date) => {
        this.setState({ issueDate: moment(date).format('DD/MM/YYYY')});
        console.log("export",this.state.issueDate);
        this.renderDatePicker3();
    };

    fetchNationalityCodeList() {
          
        axios({
            url: API.nationalityCodeList,
            method: 'get',
            headers: {
                // Authorization: `Bearer ${this.state.token}`
            }
        })
            .then((res) => {
                console.log("nationalityCodeList:", res.data.data);
                let data = []
                res.data.data.map(p => {
                    let val = { value: p.name }
                    data.push(val)
                })
                this.setState({ data: data })
            })
            .catch((error) => {
                console.log(error);
            });
    }

    componentWillMount(){
        this.fetchNationalityCodeList();
    }

    render() {
        // let data = [{
        //     value: 'Loan 2',
        // }, {
        //     value: 'Loan 2',
        // }, {
        //     value: 'Loan 3',
        // }];
          

        // console.log("PARAMS", this.props.navigation.getParam('loanApplicationId','NIL'));
        // console.log("PARAMS", this.props.navigation.getParam('token','NIL'));
        return (
            <View style={styles.container}>
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                    <StatusBar translucent={true} backgroundColor={'transparent'} />
                    <View style={{flexDirection: 'row', right:20, top: (Platform.OS == 'ios') ? 10 : 0}}>
                        <TouchableOpacity style={{marginLeft: (Platform.OS == 'ios') ? width/12: width/10, marginTop: (Platform.OS == 'ios') ? width/12: width/18}} onPress={()=> this.props.navigation.goBack()}>
                            <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        </TouchableOpacity>
                            <Text style={styles.homeTitle}>Create New Loan</Text>
                        </View>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center', flex: 1, width: width, paddingLeft: width/15}}>
                    <StepProgressLoan step={3} total={10} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', flex: 0 }}>CLIENT DETAILS</Text>

                    <ScrollView style={{ width: width / 1.1, marginTop: height / 30, alignContent: 'center'}} showsVerticalScrollIndicator={false}>

                        <View style={{ flexDirection: "row", width: width }}>
                            <FloatingLabelInput
                                label="ID Number (Auto)"
                                value={this.state.ID}
                                width={width / 1.2}
                                onChangeText={(value) => this.getID(value)}
                            />

                            <TouchableOpacity   onPress={() => this.handleChange()} style={{right: width / 15,}}>
                                <Image
                                    source={require('../assets/images/CLI3/attachment.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 15, top: height / 50 }}
                                />
                            </TouchableOpacity>
                        </View>
                        {
                            (this.state.fileAttached == true) 
                            ? 
                            <View>
                            <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 50, color: 'rgba(0,0,0,0.32)' }}>File Attached</Text>
                            <View style={{  width: width/1.2,alignItems: 'center', height: height / 7, backgroundColor: '#FAFAFA', justifyContent: 'center' }}>
                                <Image
                                    source={require('../assets/images/CLI3/file-attach.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 10, width: height / 10 }}
                                />
                                <TouchableOpacity style={{ position: 'absolute', right: width / 3, top: height / 70 }} onPress={() => this.setState({fileAttached: false})}>
                                    <Image
                                        source={require('../assets/images/CLI3/error.png')}
                                        resizeMode={"contain"}
                                        style={{ height: height / 40, width: height / 40, alignSelf: 'center' }}
                                    />
                                </TouchableOpacity>
                                
                            </View>
                            </View>
                            : null
                        }
                        
                        
                        <FloatingLabelInput
                            label="First Name"
                            value={this.state.firstName}
                            width={width / 1.2}
                            returnKeyType={"next"}
                            onSubmitEditing={() => this.customInput2.refs.innerTextInput2.focus()}
                            onChangeText={(text) => this.getFirstName(text)}
                        />
                        <FloatingLabelInput
                            label="Second Name"
                            value={this.state.secondName}
                            width={width / 1.2}
                            returnKeyType={"next"}
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            onSubmitEditing={() => this.customInput3.refs.innerTextInput3.focus()}
                            onChangeText={(text) => this.getSecondName(text)}
                        />
                        <FloatingLabelInput
                            label="Third Name"
                            value={this.state.thirdName}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}

                            onChangeText={(text) => this.getThirdName(text)}
                        />
                        <FloatingLabelInput
                            label="Last Name"
                            value={this.state.LastName}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput4 = ref}
                            refInner="innerTextInput4"
                            onChangeText={(text) => this.getLastName(text)}
                        />

                        <View style={{ flexDirection: "row" }}>
                            <FloatingLabelInput
                                label="Date of Birth"
                                ref={ref => this.customInput2 = ref}
                                refInner="innerTextInput2"
                                value={this.state.dateOfBirth}
                                width={width / 1.2}
                                editable={false}
                                onChangeText={(text) => this.getDateOfBirth(text)}
                                style={styles.passwordStyle} />

                            <TouchableOpacity onPress={() => this.renderDatePicker1()}>
                                <Image
                                    source={require('../assets/images/CLI3/calendar.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 13, right: width / 13, top: height / 120 }}
                                /> 
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.datePickerVisible1}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.renderDatePicker1}
                            />
                        </View>
                        
                        <View style={{ flexDirection: "row" }}>
                            <FloatingLabelInput
                                label="Expiry Date"
                                ref={ref => this.customInput2 = ref}
                                refInner="innerTextInput2"
                                value={this.state.expiryDate}
                                width={width / 1.2}
                                editable={false}
                                onChangeText={(text) => this.getExpiryDate(text)}
                                style={styles.passwordStyle} />

                            <TouchableOpacity onPress={() => this.renderDatePicker2()}>
                                <Image
                                    source={require('../assets/images/CLI3/calendar.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 13, right: width / 13, top: height / 120 }}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.datePickerVisible2}
                                onConfirm={this.handleDatePickedExpiry}
                                onCancel={this.renderDatePicker2}
                            />
                        </View>

                        <View style={{ flexDirection: "row" }}>
                            <FloatingLabelInput
                                label="Issue Date"
                                ref={ref => this.customInput2 = ref}
                                refInner="innerTextInput2"
                                value={this.state.issueDate}
                                width={width / 1.2}
                                editable={false}
                                onChangeText={(text) => this.getIssueDate(text)}
                                style={styles.passwordStyle} />

                            <TouchableOpacity onPress={() => this.renderDatePicker3()}>
                                <Image
                                    source={require('../assets/images/CLI3/calendar.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 13, right: width / 13, top: height / 120 }}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.datePickerVisible3}
                                onConfirm={this.handleDatePickedIssue}
                                onCancel={this.renderDatePicker3}
                            />
                        </View>
                        
                        <FloatingLabelInput
                            label="Age (Auto)"
                            value={this.state.age}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getAge(text)}
                        />
                        <View style={{width: width/1.2}}>

                        <Dropdown
                            label='Nationality'
                            data={this.state.data}
                            style={{width: 44}}
                        />
                        </View>

                        <FloatingLabelInput
                            label="Contact Number"
                            value={this.state.contactNumber}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getContactNumber(text)}
                        />

                        <FloatingLabelInput
                            label="Email id"
                            value={this.state.email}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getEmail(text)}
                        />

                        <View style={{ width: width/1.2}}>
                        <Dropdown
                            label='Academic Qualification'
                            data={this.state.data}
                        />

                        <Dropdown
                            label='Is Working?'
                            data={this.state.data}
                        />
                        </View>
                        <FloatingLabelInput
                            label="Place of Work"
                            value={this.state.placeOfWork}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getPlaceofWork(text)}
                        />
                        <FloatingLabelInput
                            label="Designation"
                            value={this.state.designation}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getDesignation(text)}
                        />
                        <FloatingLabelInput
                            label="Monthly Salary"
                            value={this.state.monthlySalary}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            // onChangeText={(text) => this.getThirdName(text)}
                        />
                        {/* <FloatingLabelInput
                            label="Facebook"
                            value={this.state.facebook}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getFacebook(text)}
                        />
                        <FloatingLabelInput
                            label="LinkedIn"
                            value={this.state.linkedIn}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getLinkedIn(text)}
                        />
                        <FloatingLabelInput
                            label="Twitter"
                            value={this.state.twitter}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getTwitter(text)}
                        />
                        <FloatingLabelInput
                            label="Google Plus"
                            value={this.state.googleplus}
                            returnKeyType={"done"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getGooglePlus                                                                                                                                     (text)}
                        /> */}

                        <LinearGradient
                            colors={['#8D218B', '#DA215B', '#F0522D']}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: 70 }}>
                            <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('CLIStepIV')}>
                                <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </ScrollView>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/12,
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height/20,
    },
    gradientStyle:{
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 40 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height/14 : height/7,
        paddingTop: height/70
    },
    headerStyle:{
        marginBottom: (Platform.OS == 'ios') ? height/50 : height/20,
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});

