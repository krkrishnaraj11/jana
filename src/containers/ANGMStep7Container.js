import React, { Component } from 'react';
import { StatusBar,Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header } from 'native-base';
import StepProgressGroupCreation from '../components/StepProgessGroupCreation';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');


const relativeList = [
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    },
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    },
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    },
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    }
]
export default class ANGMStepVII extends Component {
    constructor() {
        super();
        this.state = {
            relativeName: '',
            datePickerVisible: false,
            projStartDate: ''
        }
    }

    getRelativeName(text) {
        this.setState({
            relativeName: text
        });
    }

    getProjStartDate(text) {
        this.setState({
            projStartDate: text
        });
    }

    renderDatePicker() {
        this.setState({ datePickerVisible: !this.state.datePickerVisible });
    }

    handleDatePicked = (dob) => {
        this.setState({ projStartDate: moment(dob).format('DD/MM/YYYY')});
        console.log("export",this.state.projStartDate);
        this.renderDatePicker();
    };



    render() {
        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center',  flex:0}}>
                    <StepProgressGroupCreation step={7} total={8} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', justifyContent: 'space-evenly' }}>PROJECT DETAILS</Text>
                </View>

                <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems: 'center', marginTop: height/40}}>
                        <View style={{ flexDirection: "row", alignItems: 'center', right: width/8}}>
                            <TouchableOpacity>
                                <Image
                                    source={require('../assets/images/CLI9/checkbox.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 15, marginRight: width/60 }}
                                />
                            </TouchableOpacity>
                            <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)'}}>Project Registration Number</Text>
                        </View>
                        <FloatingLabelInput
                            label="Project Registration Number"
                            value={this.state.relativeName}
                            returnKeyType={"done"}
                            autoFocus={false}
                            width={width / 1.2}
                            onChangeText={(text) => this.getRelativeName(text)}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                        />
                        <FloatingLabelInput
                            label="Purpose of Loan"
                            value={this.state.relativeName}
                            returnKeyType={"done"}
                            autoFocus={false}
                            width={width / 1.2}
                            onChangeText={(text) => this.getRelativeName(text)}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                        />
                        <View style={{width: width/1.2}}>
                            <Dropdown
                                label='Project Type'
                                data={data}
                            />
                        </View>
                        <FloatingLabelInput
                            label="Project Preferred Name"
                            value={this.state.relativeName}
                            returnKeyType={"done"}
                            autoFocus={false}
                            width={width / 1.2}
                            onChangeText={(text) => this.getRelativeName(text)}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                        />
                        <View style={{ flexDirection: "row", width: width, marginLeft: width/5.5}}>
                            <FloatingLabelInput
                                label="Project Location"
                                // value={this.state.clientID}
                                width={width / 1.2}
                                // onChangeText={() => this.getClientID()}
                            />

                            <TouchableOpacity>
                                <Image
                                    source={require('../assets/images/CLI9/location-arrow.png') }
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 15, right: width / 15, top: height / 50 }}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: "row", marginLeft: width/10 }}>
                            <FloatingLabelInput
                                label="Project Start Date"
                                ref={ref => this.customInput2 = ref}
                                refInner="innerTextInput2"
                                value={this.state.projStartDate}
                                width={width / 1.2}
                                editable={false}
                                onChangeText={(text) => this.getProjStartDate(text)}
                                style={styles.passwordStyle} />

                            <TouchableOpacity onPress={() => this.renderDatePicker()}>
                                <Image
                                    source={require('../assets/images/CLI3/calendar.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 13, right: width / 13, top: height / 120 }}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.datePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.renderDatePicker}
                            />
                        </View>

                            <View>
                                <FloatingLabelInput
                                label="Estimated Project Running Cost"
                                value={this.state.relativeName}
                                returnKeyType={"done"}
                                autoFocus={false}
                                width={width / 1.2}
                                onChangeText={(text) => this.getRelativeName(text)}
                                ref={ref => this.customInput3 = ref}
                                refInner="innerTextInput3"
                                />

                                <FloatingLabelInput
                                label="Number of Employees"
                                value={this.state.relativeName}
                                returnKeyType={"done"}
                                autoFocus={false}
                                width={width / 1.2}
                                onChangeText={(text) => this.getRelativeName(text)}
                                ref={ref => this.customInput3 = ref}
                                refInner="innerTextInput3"
                                />

                                    <FloatingLabelInput
                                label="Number of Family Members"
                                value={this.state.relativeName}
                                returnKeyType={"done"}
                                autoFocus={false}
                                width={width / 1.2}
                                onChangeText={(text) => this.getRelativeName(text)}
                                ref={ref => this.customInput3 = ref}
                                refInner="innerTextInput3"
                                />

                                <FloatingLabelInput
                                label="Number of Non-Family Members"
                                value={this.state.relativeName}
                                returnKeyType={"done"}
                                autoFocus={false}
                                width={width / 1.2}
                                onChangeText={(text) => this.getRelativeName(text)}
                                ref={ref => this.customInput3 = ref}
                                refInner="innerTextInput3"
                                />

                                <FloatingLabelInput
                                label="Number of Women Working in Project"
                                value={this.state.relativeName}
                                returnKeyType={"done"}
                                autoFocus={false}
                                width={width / 1.2}
                                onChangeText={(text) => this.getRelativeName(text)}
                                ref={ref => this.customInput3 = ref}
                                refInner="innerTextInput3"
                                />

                                <FloatingLabelInput
                                label="Number of Saudi Employees"
                                value={this.state.relativeName}
                                returnKeyType={"done"}
                                autoFocus={false}
                                width={width / 1.2}
                                onChangeText={(text) => this.getRelativeName(text)}
                                ref={ref => this.customInput3 = ref}
                                refInner="innerTextInput3"
                                />

                                <FloatingLabelInput
                                label="Number of Non-Saudi Employees"
                                value={this.state.relativeName}
                                returnKeyType={"done"}
                                autoFocus={false}
                                width={width / 1.2}
                                onChangeText={(text) => this.getRelativeName(text)}
                                ref={ref => this.customInput3 = ref}
                                refInner="innerTextInput3"
                                />
                            <View style={{flexDirection: 'row', backgroundColor:'#FAFAFA', borderWidth:1, height: height/8, justifyContent: 'flex-start', alignItems:'center'}}>
                                <Image source={require('../assets/images/CLI9/image1.png')} style={{height: height/9, width: height/9, marginRight: height/100, marginLeft: height/100}}/>
                                <Image source={require('../assets/images/CLI9/image2.png')} style={{height: height/9, width: height/9, marginRight: height/100}}/>
                                <TouchableOpacity style={{backgroundColor:'#979797',height: height/9, width: height/9, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={require('../assets/images/CLI9/plus.png')} style={{height: height/40, width: height/40}}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <LinearGradient
                            colors={['#8D218B', '#DA215B', '#F0522D']}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height/20 }}>
                            <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('ANGMStepVIII')}>
                                <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>

                </KeyboardAwareScrollView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
