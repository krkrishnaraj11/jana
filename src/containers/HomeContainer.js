import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, View, Dimensions, TouchableOpacity,ScrollView, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Header, Card } from 'native-base';
import StepProgress from '../components/StepProgess';
var {width,height} = Dimensions.get('window');

export default class Root extends Component {
    render() {
        <StatusBar
            backgroundColor={'red'} />
        return (
            <View style={styles.container}>
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                    <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <View>
                            <Text style={styles.homeTitle}>Dashboard</Text>

                        </View>
                    </LinearGradient>
                </Header>
                <Text style={styles.statusTitle1}>MY LOAN STATUS</Text>
                <Card style={{height: height/3, width: width/1.06, alignSelf: 'center', borderRadius:5, marginTop: height/40, padding: width/35}}>
                    <View style={{flexDirection: 'row', flex:0, justifyContent: 'space-between'}}>
                        <View style={{flexDirection: 'column'}}>
                            <Text style={{fontSize: height/10, fontFamily:'Lato-Light', color: '#9575CD'}}>17</Text>
                            <Text style={{fontSize: height/66, textAlign: 'center', fontFamily:'Lato-Light', color: '#9575CD'}}>NEW</Text>
                            <Text style={{fontSize: height/66, textAlign: 'center', fontFamily:'Lato-Light', color: '#9575CD'}}>APPLICATION</Text>
                        </View>

                        <View style={{width: width/200,backgroundColor:'rgba(0,0,0,0.12)', height: height/30, alignSelf:'center' }}>
                        </View>
                        
                        <View style={{flexDirection: 'column'}}>
                            <Text style={{fontSize: height/10, fontFamily:'Lato-Light', color: '#FFA000'}}>23</Text>
                            <Text style={{fontSize: height/66,  textAlign: 'center', fontFamily:'Lato-Light', color: '#FFA000'}}>PENDING</Text>
                            <Text style={{fontSize: height/66, textAlign: 'center', fontFamily:'Lato-Light', color: '#FFA000'}}>APPLICATION</Text>
                        </View>

                        <View style={{width: width/200,backgroundColor:'rgba(0,0,0,0.12)', height: height/30, alignSelf:'center' }}/>

                        <View style={{flexDirection: 'column'}}>
                            <Text style={{fontSize: height/10, fontFamily:'Lato-Light', color: '#78909C'}}>12</Text>
                            <Text style={{fontSize: height/66,  textAlign: 'center', fontFamily:'Lato-Light', color: '#78909C'}}>CLOSED</Text>
                            <Text style={{fontSize: height/66,  textAlign: 'center', fontFamily:'Lato-Light', color: '#78909C'}}>APPLICATION</Text>
                        </View>
                    </View>

                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 0.0}}
                        style={{ height: height/17, width: width/2.33, alignItems: 'center', justifyContent: 'center', alignSelf:"center", marginTop: height/16,borderRadius:20}}>
                        <TouchableOpacity style={{height: height/18, width:width/2.4, backgroundColor:"#fff", borderRadius:20, justifyContent:"center", alignItems:"center"}} onPress={() => this.props.navigation.navigate('SelectLoan')}>
                            <Text style={{fontFamily:"Lato-Bold", fontSize:11,color:"#D72163"}}>NEW LOAN</Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </Card>
                
                <Text style={styles.statusTitle2}>RECENT ACTIVITY</Text>
                
                <Card style={{backgroundColor:'#ffffff', height: height/2.8, flex:1, height: height}}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: height/40}}>
                        <TouchableOpacity style={{width: width/1.7, marginLeft: width/20}} onPress={() => this.props.navigation.navigate('LoanDetails')}>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: 
                            height/42, color: 'rgba(0,0,0,0.87)', marginBottom: height/500}}>Loan of Mohammed Saud</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: ' rgba(0,0,0,0.32)',marginBottom: height/500}}>Approved by Operation Manager</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: 'rgba(0,0,0,0.32)'}}>28 Oct 2018, 05:05 PM</Text>
                        </TouchableOpacity>
                        
                        <View style={{width: width/3, marginTop: height/100, alignItems: 'center'}}>
                            <StepProgress step={2} total={5}/>
                        </View>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: height/40}}>
                        <TouchableOpacity style={{width: width/1.7, marginLeft: width/20}}>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: 
                            height/42, color: 'rgba(0,0,0,0.87)', marginBottom: height/500}}>Loan of Mansour Hamid</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: ' rgba(0,0,0,0.32)',marginBottom: height/500}}>Approved by Branch Manager</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: 'rgba(0,0,0,0.32)'}}>28 Oct 2018, 03:15 PM</Text>
                        </TouchableOpacity>
                        
                        <View style={{width: width/3, marginTop: height/100, alignItems: 'center'}}>
                            <StepProgress step={4} total={5}/>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: height/40}}>
                        <TouchableOpacity style={{width: width/1.7, marginLeft: width/20}}>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: 
                            height/42, color: 'rgba(0,0,0,0.87)', marginBottom: height/500}}>Loan of Shahid Al Moufti</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: ' rgba(0,0,0,0.32)',marginBottom: height/500}}>Approved by Credits Department</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: 'rgba(0,0,0,0.32)'}}>28 Oct 2018, 02:10 PM</Text>
                        </TouchableOpacity>
                        
                        <View style={{width: width/3, marginTop: height/100, alignItems: 'center'}}>
                            <StepProgress step={3} total={5}/>
                        </View>
                    </View>
                    </ScrollView>
                </Card>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/4,
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height/18,
    },
    gradientStyle:{
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 40 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height/14 : height/7,
        paddingTop: height/70
    },
    headerStyle:{
        marginBottom: (Platform.OS == 'ios') ? height/18 : height/20,
        bottom: 0
    },
    statusTitle1:{
        fontFamily:'Lato-Regular',
        fontSize: height / 44,
        color: 'rgba(0,0,0,0.32)',
        textAlign:'left',
        top: (Platform.OS == 'android') ? height/55 : 1,
        marginLeft: height/40  
    },
    statusTitle2: {
        fontFamily: 'Lato-Regular',
        fontSize: height / 44,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40,
        marginBottom: height/50
    },
});
