import React, { Component } from 'react';
import { Dimensions, Platform, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView,  StatusBar, Slider } from 'react-native';
import { Header } from 'native-base';
import axios from 'axios';
import StepProgressLoan from '../components/StepProgressLoan';
import LinearGradient from 'react-native-linear-gradient';
import { API } from '../config/Api';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');

export default class CLIStepII extends Component {
    constructor() {
        super();
        this.state = {
            selected: 'i',
            installAmount: '',
            loanCycle: '',
            loanAmount: '',
            purposeLoan: '',
            loanPeriod: 0
        }
    }

    getloanAmount(text) {
        this.setState({
            loanAmount: text
        });
    }

    getLoanCycle(text) {
        this.setState({
            loanCycle: text
        });
    }

    getPurposeLoan(text) {
        this.setState({ purposeLoan: text })
    }


    createLoanStep2(){
        let token = this.props.navigation.getParam('token','NIL');

        const fd = new FormData();
        fd.append('loan_type', this.state.selected);
        fd.append('loan_amt', this.state.loanAmount);
        fd.append('loan_period', this.state.loanPeriod);
        fd.append('loan_purpose', this.state.purposeLoan);
        fd.append('loan_cycle', this.state.loanCycle);
        console.log(fd);
        axios(API.CLIStep2, {
            method: 'post',
            data: fd,
            headers: {
                Authorization: `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        })
            .then((res) => {
                console.log(res.data);
                // this.setState({ postResponse: res.data })
                if (res.data.response_code == 'success') {
                    this.props.navigation.navigate('CLIStepIII', {
                        loanApplicationId: res.data.data.loan_application_id,
                        token: token
                    });
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }
    render() {
        // console.log("PARAMS", this.props.navigation.getParam('loanApplicationId','NIL'));
        // alert(this.props.navigation.getParam('token','NIL'))
        return (
            <View style={styles.container}>
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                    <StatusBar translucent={true} backgroundColor={'transparent'} />
                    <View style={{flexDirection: 'row', right:20, top: (Platform.OS == 'ios') ? 10 : 0}}>
                        <TouchableOpacity style={{marginLeft: (Platform.OS == 'ios') ? width/12: width/10, marginTop: (Platform.OS == 'ios') ? width/12: width/18}} onPress={()=> this.props.navigation.goBack()}>
                            <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        </TouchableOpacity>
                            <Text style={styles.homeTitle}>Create New Loan</Text>
                        </View>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center', flex:1}}>
                    <StepProgressLoan step={'2'} total={'10'} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', }}>LOAN DETAILS</Text>

                    <ScrollView style={{ width: width / 1.2, marginTop: height / 30 }} showsVerticalScrollIndicator={false}>
                        <FloatingLabelInput
                            label="Loan Amount"
                            value={this.state.loanAmount}
                            width={width / 1.2}
                            onChangeText={(value) => this.getloanAmount(value)}
                        />

                        <View style={{ flexDirection: 'row' }}>
                            {/* <FloatingLabelInput
                                label="Loan Period"
                                value={this.state.loanPeriod}
                                width={width / 3}
                                onChangeText={(value) => value = this.state.loanPeriod}
                            /> */}
                            <View style={{ flexDirection:'column'}}>
                                <Text style={{color:'#D72163',fontFamily: 'Lato-Regular', fontSize: height/48, marginBottom:5}}>Loan Period (Months)</Text>
                                <Text style={{borderWidth:1, width:width/6, height: height/20, marginLeft: 4, borderRadius: 10, textAlign: 'center',fontSize: height/30}}>{this.state.loanPeriod}</Text>
                            </View>
                            <Slider  
                                minimumTrackTintColor={'#D72163'} 
                                style={{width: width/1.7, left: 10, top:20}}
                                maximumValue={24}
                                onValueChange={(value) => {
                                    this.setState({ loanPeriod: Math.round(value)})
                                    console.log(this.state.loanPeriod)
                                }}
                            />
                        </View>

                        <FloatingLabelInput
                            label="Purpose of Loan"
                            value={this.state.purposeLoan}
                            width={width / 1.2}
                            onChangeText={(value) => this.getPurposeLoan(value)}
                        />

                        <FloatingLabelInput
                            label="Loan Cycle (Auto)"
                            value={this.state.loanCycle}
                            width={width / 1.2}
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            onChangeText={(value) => this.getLoanCycle(value)}
                        />
                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                        style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20 }}>
                        <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.createLoanStep2()}>
                            <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    </ScrollView>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/12,        
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height/20,
    },
    gradientStyle:{
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 40 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height/14 : height/7,
        paddingTop: height/70
    },
    headerStyle:{
        marginBottom: (Platform.OS == 'ios') ? height/50 : height/20,
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
