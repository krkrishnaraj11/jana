import React, { Component } from 'react';
import { Platform, AsyncStorage, ActivityIndicator, StatusBar, StyleSheet, Text, View, Dimensions, ScrollView, Image, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import { Header, Card } from 'native-base';
import StepProgress from '../components/StepProgess';
import { API } from '../config/Api';
import { token } from '../RootContainer';
var { width, height } = Dimensions.get('window');

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
      <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
  );


export default class ProfileScreen extends Component {
    constructor() {
        super();
        this.state = {
            profileData: [],
            myKey: [],
            token: ''
        }
    }


    
    fetchProfileData() {
        axios({
            url: API.profile,
            method: 'get',
            headers: {
                Authorization: `Bearer ${this.state.token}`
            }
        })
            .then((res) => {
                this.setState({
                    profileData: res.data
                })
                console.log("red",this.state.profileData)
            })
            .catch((error) => {
                console.log(error);
            });

    }

    async getItem(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            this.setState({ myKey: JSON.parse(value) });
            if (this.state.myKey.response_code == "success") {
                this.setState({
                    token: this.state.myKey.access_token
                })
                console.log(this.state.myKey.access_token)
                this.fetchProfileData();
            }
        } catch (error) {
            console.log("Error retrieving data" + error);
            alert(this.state.token);
        }
        return Promise.resolve(this.state.token)
    }

    componentWillMount() {
        console.log("AWAIT:", this.getItem('loginResponse'));
    }

    render() {

        return (
            <View style={styles.container}>
            {console.log(this.state.profileData.data)}
            <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                    <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <View style={{flexDirection: 'row', right:20, top: (Platform.OS == 'ios') ? 10 : 0}}>
                        <TouchableOpacity style={{marginLeft: height / 50, marginTop:width/12}} onPress={()=> this.props.navigation.goBack()}>
                            <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        </TouchableOpacity>
                            <Text style={styles.homeTitle}>My Profile</Text>
                        </View>
                    </LinearGradient>
                </Header>
                {(this.state.profileData.length == 0) 
                    ? <ActivityIndicator size="large" color="#8D218B" style={{ marginTop: height/3}}/>
                :
                <View style={{flex:1}}>
                <Card style={{ width: width / 1.10, alignSelf: 'center', borderRadius: 5, marginTop: (Platform.os == 'ios') ? width/100 : height/40, padding: width / 35, flex:0}}>
                    <View style={{ width: width / 1.20, alignSelf: 'center', }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={require('../assets/images/my-profile/profile-image.png')} style={{ alignSelf: 'flex-start', height: height / 7, width: height / 7 }} />
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ fontFamily: 'Lato-Regular', fontSize: 16, marginTop: height / 50, marginBottom: height / 25, marginLeft: width / 33 }}>{this.state.profileData.data.first_name} {this.state.profileData.data.last_name}</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 55, color: '#D72163', marginLeft: width / 33, textTransform: 'uppercase' }}>{this.state.profileData.data.user_type_name}</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 65, marginLeft: width / 33 }}>Joined 27 Oct 2015</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: width / 1.10, backgroundColor: "#F8FCFF", height: height / 12, borderLeftColor: '#00C8B4', borderLeftWidth: width / 100, justifyContent: 'center', alignSelf: 'center' }}>
                        <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 40, marginLeft: width / 60 }}>My Loan Status</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flex: 0, justifyContent: 'space-between', height: height / 7, marginTop: height / 50, alignItems: 'center', }}>
                        <View style={{ flexDirection: 'column', height: height / 7, justifyContent: 'center' }}>
                            <Text style={{ fontSize: height / 20, fontFamily: 'Lato-Light', color: '#9575CD', textAlign: 'center' }}>{this.state.profileData.data.new_applications}</Text>
                            <Text style={{ fontSize: height / 66, textAlign: 'center', fontFamily: 'Lato-Light', color: '#9575CD' }}>NEW</Text>
                            <Text style={{ fontSize: height / 66, textAlign: 'center', fontFamily: 'Lato-Light', color: '#9575CD' }}>APPLICATION</Text>
                        </View>

                        <View style={{ width: width / 200, backgroundColor: 'rgba(0,0,0,0.12)', height: height / 30, alignSelf: 'center' }}>
                        </View>

                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ fontSize: height / 20, fontFamily: 'Lato-Light', color: '#FFA000', textAlign: 'center' }}>{this.state.profileData.data.pending_applications}</Text>
                            <Text style={{ fontSize: height / 66, textAlign: 'center', fontFamily: 'Lato-Light', color: '#FFA000' }}>PENDING</Text>
                            <Text style={{ fontSize: height / 66, textAlign: 'center', fontFamily: 'Lato-Light', color: '#FFA000' }}>APPLICATION</Text>
                        </View>

                        <View style={{ width: width / 200, backgroundColor: 'rgba(0,0,0,0.12)', height: height / 30, alignSelf: 'center' }} />

                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ fontSize: height / 20, fontFamily: 'Lato-Light', color: '#78909C', textAlign: 'center' }}>{this.state.profileData.data.closed_applications}</Text>
                            <Text style={{ fontSize: height / 66, textAlign: 'center', fontFamily: 'Lato-Light', color: '#78909C' }}>CLOSED</Text>
                            <Text style={{ fontSize: height / 66, textAlign: 'center', fontFamily: 'Lato-Light', color: '#78909C' }}>APPLICATION</Text>
                        </View>
                    </View>

                </Card>

                <Text style={styles.statusTitle}>RECENT ACTIVITY</Text>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Card style={{ backgroundColor: '#ffffff' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: height / 40 }}>
                            <View style={{ width: width / 1.7, marginLeft: width / 20 }}>
                                <Text style={{
                                    fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                        height / 42, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                }}>Loan of Mohammed Saud</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>Approved by Operation Manager</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>28 Oct 2018, 05:05 PM</Text>
                            </View>

                            <View style={{ width: width / 3, marginTop: height / 100, alignItems: 'center' }}>
                                <StepProgress step={2} total={5} />
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: height / 40 }}>
                            <View style={{ width: width / 1.7, marginLeft: width / 20 }}>
                                <Text style={{
                                    fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                        height / 42, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                }}>Loan of Mansour Hamid</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>Approved by Branch Manager</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>28 Oct 2018, 03:15 PM</Text>
                            </View>

                            <View style={{ width: width / 3, marginTop: height / 100, alignItems: 'center' }}>
                                <StepProgress step={4} total={5} />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: height / 40 }}>
                            <View style={{ width: width / 1.7, marginLeft: width / 20 }}>
                                <Text style={{
                                    fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                        height / 42, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                }}>Loan of Shahid Al Moufti</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>Approved by Credits Department</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>28 Oct 2018, 02:10 PM</Text>
                            </View>

                            <View style={{ width: width / 3, marginTop: height / 100, alignItems: 'center' }}>
                                <StepProgress step={3} total={5} />
                            </View>
                        </View>
                    </Card>
                </ScrollView>
                </View>}

            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/8,
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height/20,
    },
    gradientStyle:{
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 40 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height/14 : height/7,
        paddingTop: height/70
    },
    headerStyle:{
        marginBottom: (Platform.OS == 'ios') ? height/20 : height/20,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: height / 44,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40,
        marginBottom: height/50
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
});
