import React, { Component } from 'react';
import { StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Header } from 'native-base';
import StepProgressLoan from '../components/StepProgressLoan';
import StepProgressGroupCreation from '../components/StepProgessGroupCreation';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');

export default class ANGMStepI extends Component {
    constructor() {
        super();
        this.state = {

        }
    }

    render() {
        return(
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>
                
                <View style={{ alignItems: 'center', height: height, width:width}}>
                    <StepProgressGroupCreation step={1} total={8} />
                    <View style={{alignItems: 'center'}}>
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', }}>BASIC DETAILS</Text>
                        <View style={{ flexDirection: "row", width: width, justifyContent:'center', marginLeft: width/20, marginTop: height/40 }}>
                            <FloatingLabelInput
                                label= "Enter Client ID"
                                value={this.state.clientID}
                                width={width / 1.2}
                                onChangeText={() => this.getClientID()}
                            />

                                <Image
                                    source={require('../assets/images/select-loan/warn.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 15, right: width / 15, top: height / 200 }}
                                />
                        </View>
                        <Text style={{ fontFamily: 'Lato-Regular', textAlign: 'right', bottom: height / 50, color: '#F0522D', fontSize: height / 60, width: width/1.2 }}>No Records Found</Text>
                    </View>
                <LinearGradient
                            colors={['#8D218B', '#DA215B', '#F0522D']}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20 }}>
                            <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => (this.state.selected == 'individual') ? this.props.navigation.navigate('CLIStepII') : this.props.navigation.navigate('ANGMStepII')}>
                                <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                </View>
            </View>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems:'center',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
