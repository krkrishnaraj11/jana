import React, { Component } from 'react';
import { Dimensions, AsyncStorage, Text, View, Image, TouchableOpacity, FlatList, StatusBar, Platform, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Header } from 'native-base';
import Svg, { Circle, Defs, Stop } from 'react-native-svg';
import moment from 'moment';
import axios from 'axios';
import { API } from '../config/Api';
import PercentageCircle from 'react-native-percentage-circle';
var { height, width } = Dimensions.get('window');
import { token } from '../RootContainer';
import StepProgress from '../components/StepProgess';
import { styles } from './styles/LoanContainerStyles';
import { TextInput } from 'react-native-gesture-handler';
var data = [
    {
        title: 'Loan of Shahid Al Moufti',
        subTitle: 'Approved by Credits Department',
        dateTime: '28 Oct 2018, 02:10 PM',
        step: 3,
        percent: 90
    },
    {
        title: 'Loan of Ahammed Moufti',
        subTitle: 'Approved by Credits Department',
        dateTime: '28 Oct 2018, 02:10 PM',
        step: 3,
        percent: 55
    }
]

export default class Loan extends Component {
    constructor() {
        super();
        this.arrayholder = []
        this.state = {
            token: '',
            myKey: {},
            loanList: {},
            search: true,
            searchValue: '',
            roll: false,
            list: {}
        }
    }

    searchFilterFunction = text => {
        this.setState({ searchValue: text })
        const newData = this.arrayholder.data.filter(item => {
            console.log((item.first_name || "").toUpperCase())
            const itemData = `${(item.first_name || "").toUpperCase()} ${(item.second_name || "").toUpperCase()}   
          ${(item.third_name || "").toUpperCase()} ${(item.last_name || "").toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({ loanList: newData, roll: true });
        console.log(this.state.loanList)
    };


    async getItem(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            this.setState({ myKey: JSON.parse(value) });
            if (this.state.myKey.response_code == "success") {
                this.setState({
                    token: this.state.myKey.access_token
                })
                this.fetchLoanList();
            }
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
        return Promise.resolve(this.state.token)
    }

    componentDidMount() {
        console.log("login:", this.getItem('loginResponse'));
    }

    fetchLoanList() {
        console.log(this.state.token);
        axios({
            url: API.loanList,
            method: 'get',
            headers: {
                Authorization: `Bearer ${this.state.token}`
            }
        })
            .then((res) => {
                this.arrayholder = res.data;
                this.setState({ loanList: this.arrayholder.data });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    renderLoanList(list) {
        console.log(list)
        var date = moment(list.item.activity_datetime).format('D MMM YYYY, h:mm A');
        return (
            <View>
                <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: height / 40 }} onPress={() => this.props.navigation.navigate('LoanDetails')}>
                    <View style={{ height: height / 12, width: height / 12, marginLeft: width / 20, marginRight: width / 20 }}>
                        <PercentageCircle radius={27} percent={list.item.percent_complete} color={'black'} style={{ overflow: 'hidden' }} />
                    </View>
                    <View style={{ width: width / 1.7, marginLeft: width / 20 }}>
                        <Text style={{
                            fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                height / 42, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                        }}>Loan of {list.item.first_name} {list.item.second_name} {list.item.third_name} {list.item.last_name}</Text>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>{list.item.subTitle}</Text>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>{date}</Text>
                    </View>

                    <View style={{ width: width / 4, marginTop: height / 100, alignItems: 'center', marginRight: 30, marginLeft: 10 }}>
                        <StepProgress step={list.item.step} total={5} />
                    </View>
                </TouchableOpacity>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'center', marginTop: height / 50 }} />
            </View>
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        {(this.state.search)
                            ?
                            <View style={{ flexDirection: 'row', bottom: height / 200 }}>
                                <Text style={styles.homeTitle}>Loans</Text>
                                <View style={{ flexDirection: 'row', left: (Platform.OS == 'android') ? width / 1.4 : width / 1.4, justifyContent: 'space-around', alignSelf: 'center', marginTop: (Platform.OS == 'android') ? height / 25 : height / 20 }}>
                                    <TouchableOpacity onPress={() => this.setState({ search: false })}>
                                        <Image source={require('../assets/images/loans/search.png')} style={styles.searchIconStyle} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SelectLoan')}>
                                        <Image source={require('../assets/images/loans/add.png')} style={styles.plusIconStyle} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : <View style={{ height: height / 18, width: width / 1.05, backgroundColor: '#fff', alignSelf: 'center', marginTop: height / 24, flexDirection: 'row' }}>
                                <Image source={require('../assets/images/loans/search-black.png')} style={{
                                    height: height / 35,
                                    width: height / 35,
                                    marginTop: height / 60,
                                    marginLeft: height / 80,
                                }} />
                                <TextInput
                                    style={{
                                        marginLeft: width / 40, fontFamily: 'Lato-Regular',
                                        color: 'rgba(0,0,0,0.38)', fontSize: height / 40
                                    }}
                                    value={this.state.searchValue}
                                    width={width / 1.3}
                                    onChangeText={(text) => this.searchFilterFunction(text)}
                                />

                                <TouchableOpacity onPress={() => this.setState({ search: true, data: this.arrayholder })}>
                                    <Image source={require('../assets/images/loans/close.png')} style={{
                                        height: height / 35,
                                        width: height / 35,
                                        marginTop: height / 60,
                                    }} />
                                </TouchableOpacity>
                            </View>
                        }

                    </LinearGradient>
                </Header>

                <View style={{ height: height, marginTop: (Platform.OS == 'android') ? 0 : 0, width: width, backgroundColor: '#fff' }}>
                    {(this.state.loanList == null)
                        ?
                        <ActivityIndicator size="large" color="#8D218B" style={{ marginTop: height / 3 }} />
                        :

                        <FlatList
                            data={this.loanList}
                            renderItem={(item) => this.renderLoanList(item)}
                        />
                    }
                </View>
            </View>
        );
    }
}

