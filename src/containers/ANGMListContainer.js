import React, { Component } from 'react';
import { StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import { Header } from 'native-base';
import StepProgressLoanGroup from '../components/StepProgessLoanGroup';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');


const relativeList = [
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    },
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    },
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    },
    {
        name: 'Mohammed Bin Shah',
        relation: 'Brother',
        phone: '7907571692'
    }
]
export default class ANGMemberList extends Component {
    constructor() {
        super();
        this.state = {
            relativeName: '',
            relativeRelation: '',
            relativeContact: '',
            relativeList : [
                {
                    name: 'Mohammed Bin Shah',
                    relation: 'Brother',
                    phone: '7907571692'
                },
                {
                    name: 'Mohammed Bin Shah',
                    relation: 'Brother',
                    phone: '7907571692'
                },
                {
                    name: 'Mohammed Bin Shah',
                    relation: 'Brother',
                    phone: '7907571692'
                },
                {
                    name: 'Mohammed Bin Shah',
                    relation: 'Brother',
                    phone: '7907571692'
                }
            ]
        }
    }

    getRelativeName(text) {
        this.setState({
            relativeName: text
        });
    }

    getRelativeRelation(text) {
        this.setState({
            relativeRelation: text
        });
    }

    getRelativeContact(text) {
        this.setState({
            relativeContact: text
        })
    }

    updateRelativeList(){
        var item = {
            name: this.state.relativeName,
            relation: this.state.relativeRelation,
            phone: this.state.relativeContact
        }
        
        this.setState({ relativeList: [...this.state.relativeList, item]});
        console.log(this.state.relativeList)
    }

    renderRelativeList(data){
        return(
            <View style={{flexDirection: 'column'}}>    
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{width: width/1.5}}>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: 16, color: 'rgba(0,0,0,0.87)'}}>{data.item.name}</Text>
                        <Text style={{fontFamily: 'Roboto-Regular', fontSize: 13, color: 'rgba(0,0,0,0.32)'}}>{data.item.relation}</Text>
                        <Text style={{fontFamily: 'Roboto-Regular', fontSize: 13, color: 'rgba(0,0,0,0.32)'}}>{data.item.phone}</Text>
                    </View>
                    <TouchableOpacity>
                        <Image source={require('../assets/images/CLI6/more.png')} style={{resizeMode: 'contain', height: height/12, width: width/15}}/>
                    </TouchableOpacity>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'center', marginTop: height / 50, marginBottom: height/70}} />
            </View>
        )
    }

    render() {
        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center',  flex:1}}>
                    <StepProgressLoanGroup step={2} total={2} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', justifyContent: 'space-evenly', marginBottom: height/40 }}>MEMBERS</Text>

                    <KeyboardAwareScrollView enableOnAndroid style={{ width: width / 1.1, marginTop: height / 70}} showsVerticalScrollIndicator={false}>

                    <FlatList
                        data={this.state.relativeList}
                        renderItem={(data) => this.renderRelativeList(data)}/>

                        <LinearGradient
                            colors={['#8D218B', '#DA215B', '#F0522D']}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height/20 }}>
                            <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('Loan')}>
                                <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>COMPLETE</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </KeyboardAwareScrollView>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
