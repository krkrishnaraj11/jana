import React, { Component } from 'react';
import { Dimensions, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Header } from 'native-base';
import StepProgressLoan from '../components/StepProgressLoan';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');

export default class CLIStepV extends Component {
    constructor() {
        super();
        this.state = {
            IBAN: '',
            IBANConfirm: '0',
            fileAttached: true,
            clientBank: '',
            socialNo: '',
            MSIncome: ''
        }
    }

    getSocialNo(text) {
        this.setState({
            socialNo: text
        });
    }

    getMSIncome(text) {
        this.setState({
            MSIncome: text
        });
    }

    render() {
        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Create New Loan</Text>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center',  flex:1}}>
                    <StepProgressLoan step={5} total={10} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', justifyContent: 'space-evenly' }}>CLIENT PERSONAL DETAILS</Text>

                    <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ width: width / 1.1, marginTop: height / 70}}>
                        <View style={{alignItems:'center'}}>
                        <FloatingLabelInput
                            label="Region"
                            value={this.state.googleplus}
                            returnKeyType={"done"}
                            autoFocus={false}
                            // style={{justifyContent: 'center'}}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                        />

                        <FloatingLabelInput
                            label="City"
                            value={this.state.googleplus}
                            returnKeyType={"done"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                        />      

                        <FloatingLabelInput
                            label="Neighbourhood"
                            value={this.state.googleplus}
                            style={{alignSelf: 'center'}}
                            returnKeyType={"done"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                        />
                        </View>

                        <View style={{width: width/1.2, alignSelf: 'center'}}>
                            <Dropdown
                                label='Living In'
                                data={data}
                            />

                            <Dropdown
                                label='Marital Status'
                                data={data}
                            />
                            
                            <Dropdown
                                label='Number of Dependencies'
                                style={{width: width/3}}
                                data={data}
                            /> 
                            <View style={{ flexDirection: "row", width: width }}>
                                <FloatingLabelInput
                                    label="Social No"
                                    value={this.state.socialNo}
                                    width={width / 1.2}
                                onChangeText={() => this.getIBAN()}
                                />

                                <TouchableOpacity>
                                    <Image
                                        source={require('../assets/images/CLI5/check-circle.png')}
                                        resizeMode={"contain"}
                                        style={{ height: height / 13, width: width / 15, right: width / 15, top: height / 50 }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <FloatingLabelInput
                                    label="Monthly Social Income"
                                    value={this.state.MSIncome}
                                    width={width / 1.2}
                                onChangeText={(text) => this.getMSIncome(text)}
                                /> 
                        </View>

                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                        style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height/20 }}>
                        <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('CLIStepVI')}>
                            <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    </KeyboardAwareScrollView>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
