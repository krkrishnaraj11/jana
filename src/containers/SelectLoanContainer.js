import React, { Component } from 'react';
import { AsyncStorage, StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Platform } from 'react-native';
import { API } from '../config/Api';
import axios from 'axios';
import { Header } from 'native-base';
import StepProgressLoan from '../components/StepProgressLoan';
import StepProgressLoanGroup from '../components/StepProgessLoanGroup';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');

export default class SelectLoan extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            clientID: '',
            selected: 'i',
            loanProduct: '',
            loanId: 'D345',
            loanBranch: 1,
            loanStatus: 'd',
            postResponse: {},
            myKey: {},
            token: ''
        }
    }

    getClientID(text) {
        this.setState({
            clientID: text
        });
        console.log(this.state.clientID)
    }

    async storeItem(key, item) {
        try {
            //we want to wait for the Promise returned by AsyncStorage.setItem()
            //to be resolved to the actual value before returning the value
            var jsonOfItem = await AsyncStorage.setItem(key, item);
            return jsonOfItem;
        } catch (error) {
            console.log(error.message);
        }
    }


    async createLoanStep1() {
        const value = await AsyncStorage.getItem('loginResponse');
        this.setState({ myKey: JSON.parse(value) });
        console.log("$$", this.state.myKey)
        if (this.state.myKey.response_code == "success") {
            this.setState({ token: this.state.myKey.access_token });
            console.log(this.state.token);
        }

        const fd = new FormData();
        fd.append('loan_id', this.state.clientID);
        fd.append('loan_type', this.state.selected);
        fd.append('loan_product', this.state.loanProduct);
        fd.append('loan_branch', this.state.loanBranch);
        fd.append('loan_status', this.state.loanStatus);
        console.log(fd);
        axios(API.selectLoan, {
            method: 'post',
            data: fd,
            headers: {
                Authorization: `Bearer ${this.state.token}`,
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        })
            .then((res) => {
                console.log(res.data);
                this.setState({ postResponse: res.data })
                if (res.data.response_code == 'success') {
                    this.props.navigation.navigate('CLIStepII', {
                        loanApplicationId: res.data.data.loan_application_id,
                        token: this.state.token
                    });
                }
                else {
                    this.props.navigation.navigate('ManageGroupMember');
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    fetchLoanList() {
        axios({
            url: API.loanProductList,
            method: 'get',
            headers: {
                // Authorization: `Bearer ${this.state.token}`
            }
        })
            .then((res) => {
                console.log("LoanProductList:", res.data.data);
                let data = []
                res.data.data.map(p => {
                    let val = { value: p.name }
                    data.push(val)
                })
                this.setState({ data: data })
            })
            .catch((error) => {
                console.log(error);
            });
    }


    componentWillMount() {
        this.fetchLoanList();
    }
    render() {

        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <View style={{ flexDirection: 'row', right: 20, top: (Platform.OS == 'ios') ? 10 : 0 }}>
                            <TouchableOpacity style={{ marginLeft: (Platform.OS == 'ios') ? width / 12 : width / 10, marginTop: (Platform.OS == 'ios') ? width / 12 : width / 18 }} onPress={() => this.props.navigation.goBack()}>
                                <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                            </TouchableOpacity>
                            <Text style={styles.homeTitle}>Create New Loan</Text>
                        </View>
                    </LinearGradient>
                </Header>

                <KeyboardAwareScrollView>
                    {(this.state.selected == 'i') ? < StepProgressLoan step={'1'} total={'10'} /> : <StepProgressLoanGroup step={'1'} total={'2'} />}
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', alignSelf: 'center' }}>SELECT LOAN TYPE</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.setState({ selected: 'i' })}>
                            <Image source={(this.state.selected == 'i') ? require('../assets/images/select-loan/individual.png') : require('../assets/images/select-loan/individual-inactive.png')} style={{ height: height / 10, width: height / 10, resizeMode: 'contain', marginTop: height / 35 }} />
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 33, color: (this.state.selected == 'i') ? '#D72163' : 'rgba(0,0,0,0.32)', textAlign: 'center' }}>Individual</Text>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 45, color: (this.state.selected == 'i') ? '#D72163' : 'rgba(0,0,0,0.32)', textAlign: 'center' }}>Loan Application</Text>
                        </TouchableOpacity>

                        <View style={{ width: width / 200, backgroundColor: 'rgba(0,0,0,0.12)', height: height / 7, alignSelf: 'center', margin: 23 }} />

                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.setState({ selected: 'g' })}>
                            <Image source={(this.state.selected == 'g') ? require('../assets/images/select-loan/group-active.png') : require('../assets/images/select-loan/group.png')} style={{ height: height / 10, width: height / 10, resizeMode: 'contain', marginTop: height / 35 }} />
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 33, color: (this.state.selected == 'g') ? '#D72163' : 'rgba(0,0,0,0.32)', textAlign: 'center' }}>Group</Text>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 45, color: (this.state.selected == 'g') ? '#D72163' : 'rgba(0,0,0,0.32)', textAlign: 'center' }}>Loan Application</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: width / 1.2, marginTop: height / 30, alignSelf: 'center' }}>
                        <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 35, color: 'rgba(0,0,0,0.54)', alignSelf: 'center' }}>
                            Loan Details
                        </Text>
                        <Dropdown
                            label='Choose Loan Product'
                            dropdownPosition={-2}
                            data={this.state.data}
                            value={this.state.loanProduct}
                            onChangeText={(value) => {
                                console.log(value);
                                let a = this.state.data.map(p => p.value)
                                console.log(a)
                                this.setState({ loanProduct: 1 + a.indexOf(value)})
                                console.log(this.state.loanProduct)
                            }}
                            
                        />
                        <View style={{ flexDirection: "row", width: width }}>
                            <FloatingLabelInput
                                label={this.state.selected == 'i' ? "Enter Client ID" : "Enter Group Name"}
                                value={this.state.clientID}
                                width={width / 1.2}
                                onChangeText={(value) => this.getClientID(value )}
                            />

                            {this.state.selected == 'i' ? 
                                // <Image
                                //     source={(this.state.passSecure) ? null : require('../assets/images/select-loan/warn.png')}
                                //     resizeMode={"contain"}
                                //     style={{ height: height / 13, width: width / 15, right: width / 15, top: height / 200 }}
                                // />
                                null
 : null}
                        </View>
                        {this.state.selected == 'i' ?
                            // <Text style={{ fontFamily: 'Lato-Regular', textAlign: 'right', bottom: height / 50, color: '#F0522D', fontSize: height / 60 }}>No Records Found</Text>
                            null : null}
                        <LinearGradient
                            colors={['#8D218B', '#DA215B', '#F0522D']}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20 }}>
                            <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}
                                // onPress={() => (this.state.selected == 'i') ? this.props.navigation.navigate('CLIStepII') : this.props.navigation.navigate('ManageGroupMember')}

                                onPress={() => this.createLoanStep1()}>
                                <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        left: width / 12,
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height / 20,
    },
    gradientStyle: {
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 40 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height / 14 : height / 7,
        paddingTop: height / 70
    },
    headerStyle: {
        marginBottom: (Platform.OS == 'ios') ? height / 50 : height / 20,
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
