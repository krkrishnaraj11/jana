import React, { Component } from 'react';
import { Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header, Card } from 'native-base';
import StepProgressLoan from '../components/StepProgressLoan';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');


export default class LoanSuccess extends Component {
    constructor(){
        super();
        this.state = {
        }
    }

    render(){
        return(
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Create New Loan</Text>
                    </LinearGradient>
                </Header>

                <Card style={{ alignItems: 'center',  flex:0, backgroundColor: '#fff', marginTop: height/40, width: width/1.03, alignSelf:'center', height: height/3, alignItems:'center'}}>
                    <Image source={require('../assets/images/loan-success/success.png')} style={{ height: height/15, width: height/15, marginTop: height/15, marginBottom:height/40}}/>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'center', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.87)',marginBottom: height/100}}>Loan Application Created</Text>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/45, textAlign:'center', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)',marginBottom: height/150}}>Ref No: JANA000222145</Text>
                    <TouchableOpacity style={{borderBottomWidth:1, borderBottomColor: '#D72163'}}>
                        <Text style={{fontFamily: 'Lato-Light', fontSize: height/50, textAlign:'center', alignSelf:'center', color: '#D72163'}}>Track this Loan Application</Text>
                    </TouchableOpacity>
                </Card>
            </View>

        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});