import React, { Component } from 'react';
import { StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Header } from 'native-base';
import StepProgressLoan from '../components/StepProgressLoan';
import StepProgressLoanGroup from '../components/StepProgessLoanGroup';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');

export default class ManageGroupMember extends Component {
    constructor() {
        super();
        this.state = {

        }
    }

    render() {
        return(
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>
                
                <View style={{ alignItems: 'center', height: height, width:width}}>
                    <StepProgressLoanGroup step={2} total={2} />
                    <View style={{alignItems: 'center', marginTop: height/8}}>
                        <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', marginBottom: height/75}}>MANAGE GROUP MEMBERS</Text>
                        <TouchableOpacity style={{backgroundColor:'#D72163', height: height/15, justifyContent:'center', width: width/1.1, alignItems:'center', borderRadius: width/75, marginBottom: height/75}}>
                            <Text style={{fontFamily: 'Lato-Bold', color:'#fff', fontSize: height/50}}>ADD GROUP LEADER</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{backgroundColor:'#D72163', height: height/15, justifyContent:'center', width: width/1.1, alignItems:'center', borderRadius: width/75, marginBottom: height/75}}>
                            <Text style={{fontFamily: 'Lato-Bold', color:'#fff', fontSize: height/50}}>ADD GROUP TELLER</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{backgroundColor:'#D72163', height: height/15, justifyContent:'center', width: width/1.1, alignItems:'center', borderRadius: width/75}} onPress={() => this.props.navigation.navigate('ANGMStepI')}>
                            <Text style={{fontFamily: 'Lato-Bold', color:'#fff', fontSize: height/50}}>ADD GROUP MEMBER</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems:'center',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
