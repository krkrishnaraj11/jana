import React, { Component } from 'react';
import { StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header } from 'native-base';
import StepProgressGroupCreation from '../components/StepProgessGroupCreation';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');

export default class ANGMStepIV extends Component {
    constructor() {
        super();
        this.state = {
            IBAN: '',
            IBANConfirm: '0',
            fileAttached: true,
            clientBank: ''
        }
    }

    getInstallAmount(text) {
        this.setState({
            installAmount: text
        });
    }

    getLoanCycle(text) {
        this.setState({
            loanCycle: text
        });
    }

    render() {
        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center', flex: 1, paddingLeft: width / 20 }}>
                    <StepProgressGroupCreation step={4} total={8} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', justifyContent: 'space-evenly' }}>MEMBER BANK DETAILS</Text>

                    <ScrollView showsVerticalScrollIndicator={false} style={{ width: width / 1.1, marginTop: height / 100 }}>
                        <View style={{ flexDirection: "row", width: width }}>
                            <FloatingLabelInput
                                label="IBAN"
                                value={this.state.IBAN}
                                width={width / 1.2}
                                onChangeText={() => this.getIBAN()}
                            />

                            <TouchableOpacity>
                                <Image
                                    source={require('../assets/images/CLI3/attachment.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 15, right: width / 15, top: height / 50 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <FloatingLabelInput
                            label="Confirm IBAN"
                            width={width / 1.2}
                        />
                        <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 50, color: 'rgba(0,0,0,0.32)' }}>File Attached</Text>
                        <View style={{ alignItems: 'center', height: height / 7, backgroundColor: '#FAFAFA', justifyContent: 'center', width: width / 1.2 }}>
                            <Image
                                source={require('../assets/images/CLI3/file-attach.png')}
                                resizeMode={"contain"}
                                style={{ height: height / 10, width: height / 10 }}
                            />
                            <TouchableOpacity style={{ position: 'absolute', right: width / 3, top: height / 70 }}>
                                <Image
                                    source={require('../assets/images/CLI3/error.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 40, width: height / 40, alignSelf: 'center', left: height / 50 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: width / 1.2 }}>

                            <Dropdown
                                label='Nationality'
                                data={data}
                                style={{ width: 44 }}
                            />
                        </View>
                        <LinearGradient
                            colors={['#8D218B', '#DA215B', '#F0522D']}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height / 20 }}>
                            <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('ANGMStepV')}>
                                <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </ScrollView>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
