import React, { Component } from 'react';
import { StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header } from 'native-base';
import StepProgessGroupCreation from '../components/StepProgessGroupCreation';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import FloatingLabelInput from '../components/FloatingLabelInput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
var { height, width } = Dimensions.get('window');

export default class ANGMStepII extends Component {
    constructor() {
        super();
        this.state = {
            installAmount: '',
            loanCycle: '',
            loanAmount:''
        }
    }

    getInstallAmount(text) {
        this.setState({
            installAmount: text
        });
    }

    getLoanAmount(text) {
        this.setState({
            loanAmount: text
        });
    }
    getLoanCycle(text) {
        this.setState({
            loanCycle: text
        });
    }

    render() {
        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center' , flex:1}}>
                    <StepProgessGroupCreation step={2} total={8} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', }}>LOAN DETAILS</Text>
                    <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ width: width / 1.2, marginTop: height / 30 }}>
                        <FloatingLabelInput
                            label="Enter Loan Amount"
                            value={this.state.loanAmount}
                            width={width / 1.2}
                            onSubmitEditing={() => this.customInput2.refs.innerTextInput2.focus()}
                            onChangeText={() => this.getLoanAmount()}
                        />

                        <Dropdown
                            label='Number of Installments'
                            data={data}
                        />

                        <FloatingLabelInput
                            label="Installment Amount"
                            value={this.state.installAmount}
                            width={width / 1.2}
                            onSubmitEditing={() => this.customInput2.refs.innerTextInput2.focus()}
                            onChangeText={() => this.getInstallAmount()}
                        />

                        <FloatingLabelInput
                            label="Loan Cycle (Auto)"
                            value={this.state.loanCycle}
                            width={width / 1.2}
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            onChangeText={() => this.getLoanCycle()}
                        />
                    </View>
                    <LinearGradient
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                        style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom:49 }}>
                        <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('ANGMStepIII')}>
                            <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    </ScrollView>
                    
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
