import React, { Component } from 'react';
import { AsyncStorage, StatusBar, StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, Platform } from 'react-native';
import axios from 'axios';
import { API } from '../config/Api';
import LinearGradient from 'react-native-linear-gradient';
import { Header } from 'native-base';
var { height, width } = Dimensions.get('window');
import { token } from '../RootContainer';
export default class More extends Component {

    constructor() {
        super();
        this.state = {
            myKey: {},
            token: ''
        }
    }
    async removeItemValue(key) {
        try {
            await AsyncStorage.removeItem(key);
            return true;
        }
        catch (exception) {
            return false;
        }
    }

    logout = () => {
        console.log(this.state.token);
        axios({
            url: API.logout,
            method: 'post',
            headers: {
                Authorization: `Bearer ${this.state.token}`
            }
        })
            .then((res) => {
                this.removeItemValue('loginResponse');
                this.props.navigation.navigate('LogOut');
            })
            .catch((error) => {
                console.log(error);
            });
    }


    async getItem(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            this.setState({ myKey: JSON.parse(value) });
            if (this.state.myKey.response_code == "success") {
                this.setState({
                    token: this.state.myKey.access_token
                })
            }
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
        return Promise.resolve(this.state.token)
    }

    componentWillMount() {
        console.log("AWAIT:", this.getItem('loginResponse'));
    }
    
    render() {
        return (
            <View>
                <StatusBar
                    backgroundColor={'transparent'} />
                <Header style={styles.headerStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                    <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <View>
                            <Text style={styles.homeTitle}>More</Text>

                        </View>
                    </LinearGradient>
                </Header>
                <View style={{ flexDirection: 'column', marginTop: height / 70 }}>
                    <TouchableOpacity style={{ flexDirection: 'row', backgroundColor: '#FFFFFF', justifyContent: 'space-between' }} onPress={() => this.props.navigation.navigate('Profile')}>
                        <Image source={require('../assets/images/more/myprofile_active.png')} resizeMode={'contain'} style={{ height: height / 20, width: height / 20, margin: height / 45 }} />
                        <View style={{ flexDirection: 'column', margin: height / 45, width: width / 1.9 }}>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 40, color: '#D72163' }}>My Profile</Text>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 40, color: 'rgba(20,20,20,0.38)' }}>View Profile Details</Text>
                        </View>
                        <Image source={require('../assets/images/more/right.png')} resizeMode={'contain'} style={{ height: height / 20, width: height / 20, margin: height / 45 }} />
                    </TouchableOpacity>
                    <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'center' }} />

                    <TouchableOpacity style={{ flexDirection: 'row', backgroundColor: '#FFFFFF', justifyContent: 'space-between' }}>
                        <Image source={require('../assets/images/more/unlock.png')} resizeMode={'contain'} style={{ height: height / 20, width: height / 20, margin: height / 45 }} />
                        <View style={{ flexDirection: 'column', margin: height / 45, width: width / 1.9 }}>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 40, color: 'rgba(0,0,0,0.54)' }}>Change Password</Text>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 40, color: 'rgba(20,20,20,0.38)' }}>View Profile Details</Text>
                        </View>
                        <Image source={require('../assets/images/more/right.png')} resizeMode={'contain'} style={{ height: height / 20, width: height / 20, margin: height / 45 }} />
                    </TouchableOpacity>
                    <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'center' }} />

                    <TouchableOpacity style={{ flexDirection: 'row', backgroundColor: '#FFFFFF', justifyContent: 'space-between' }} onPress={() => this.logout()}>
                        <Image source={require('../assets/images/more/logout.png')} resizeMode={'contain'} style={{ height: height / 20, width: height / 20, margin: height / 45 }} />
                        <View style={{ flexDirection: 'column', margin: height / 45, width: width / 1.9 }}>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 40, color: 'rgba(0,0,0,0.54)' }}>Logout</Text>
                            <Text style={{ fontFamily: 'Lato-Regular', fontSize: height / 40, color: 'rgba(20,20,20,0.38)' }}>Sign Out from this Account</Text>
                        </View>
                        <Image source={require('../assets/images/more/right.png')} resizeMode={'contain'} style={{ height: height / 20, width: height / 20, margin: height / 45 }} />
                    </TouchableOpacity>
                    <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'center' }} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent:'center',
        fontFamily:'Lato-Bold',
        color:'#fff',
        textAlign:'left',
        left: width/4,
        top: (Platform.OS == 'android') ? StatusBar.currentHeight : height/20,
    },
    gradientStyle:{
        height: 56,
        width: 450,
        bottom: (Platform.OS == 'ios') ? 30 : 0,
        height: (Platform.OS == 'android') ? StatusBar.currentHeight + height/14 : height/8,
        paddingTop: height/70
    },
    headerStyle:{
        marginBottom: (Platform.OS == 'ios') ? height/30 : height/20,
    },
    statusTitle:{
        fontFamily:'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign:'left',
        top: (Platform.OS == 'android') ? height/55 : 1,
        marginLeft: height/40  
    }
});
