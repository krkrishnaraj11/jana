import React, { Component } from 'react';
import { StatusBar, Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header } from 'native-base';
import moment from 'moment';
import StepProgressGroupCreation from '../components/StepProgessGroupCreation';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import FloatingLabelInput from '../components/FloatingLabelInput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
var { height, width } = Dimensions.get('window');

export default class ANGMStepIII extends Component {
    constructor() {
        super();
        this.state = {
            firstName: '',
            secondName: '',
            thirdName: '',
            LastName: '',
            expiryDate: '',
            dateOfBirth: '',
            datePickerVisible: false,
            age: '',
            contactNumber: '',
            email: '',
            nationality:'',
            qualifcation: '',
            isWorking: false,
            placeOfWork: '',
            designation: '',
            monthlySalary: '',
            facebook: '',
            linkedIn: '',
            twitter: '',
            googleplus: ''
        }
    }

    getFirstName(text) {
        this.setState({
            firstName: text
        });
    }

    getSecondName(text) {
        this.setState({
            secondName: text
        });
    }

    getThirdName(text) {
        this.setState({
            thirdName: text
        });
    }

    getLastName(text) {
        this.setState({
            LastName: text
        });
    }

    getExpiryDate(text) {
        this.setState({
            expiryDate: text
        });
    }

    getDateOfBirth(text) {
        this.setState({
            dateOfBirth: text
        });
    }

    getAge(text) {
        this.setState({ age: text })
    }
    
    getEmail(text){
        this.setState({ email: text })
    }

    getContactNumber(text) {
        this.setState({ contactNumber: text })
    }

    getPlaceofWork(text) {
        this.setState({ placeOfWork: text })
    }

    getDesignation(text) {
        this.setState({ designation: text })
    }

    getMonthlySalary(text) {
        this.setState({ monthlySalary: text })
    }
    getFacebook(text) {
        this.setState({ facebook: text })
    }

    getLinkedIn(text) {
        this.setState({ linkedIn: text })
    }

    getTwitter(text) {
        this.setState({ twitter: text })
    }

    getGooglePlus(text) {
        this.setState({ googleplus: text })
    }

    renderDatePicker() {
        this.setState({ datePickerVisible: !this.state.datePickerVisible });
    }

    handleDatePicked = (dob) => {
        this.setState({ dateOfBirth: moment(dob).format('DD/MM/YYYY')});
        console.log("export",this.state.dateOfBirth);

        this.renderDatePicker();
    };

    handleDatePickedExpiry = (date) => {
        this.setState({ expiryDate: moment(date).format('DD/MM/YYYY')});
        console.log("export",this.state.expiryDate);
        this.renderDatePicker();
    };

    render() {
        let data = [{
            value: 'Loan 1',
        }, {
            value: 'Loan 2',
        }, {
            value: 'Loan 3',
        }];

        return (
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                        <StatusBar translucent={true} backgroundColor={'transparent'} />
                        <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        <Text style={styles.homeTitle}>Add New Group Member</Text>
                    </LinearGradient>
                </Header>

                <View style={{ alignItems: 'center', flex: 1, width: width, paddingLeft: width/15}}>
                    <StepProgressGroupCreation step={3} total={8} />
                    <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)', flex: 0 }}>MEMBER DETAILS</Text>

                    <ScrollView style={{ width: width / 1.1, marginTop: height / 30, alignContent: 'center'}} showsVerticalScrollIndicator={false}>

                        <View style={{ flexDirection: "row", width: width }}>
                            <FloatingLabelInput
                                label="ID Number (Auto)"
                                // value={this.state.clientID}
                                width={width / 1.2}
                                // onChangeText={() => this.getClientID()}
                            />

                            <TouchableOpacity>
                                <Image
                                    source={require('../assets/images/CLI3/attachment.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 15, right: width / 15, top: height / 50 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 50, color: 'rgba(0,0,0,0.32)' }}>File Attached</Text>
                        <View style={{  width: width/1.2,alignItems: 'center', height: height / 7, backgroundColor: '#FAFAFA', justifyContent: 'center' }}>
                            <Image
                                source={require('../assets/images/CLI3/file-attach.png')}
                                resizeMode={"contain"}
                                style={{ height: height / 10, width: height / 10 }}
                            />
                            <TouchableOpacity style={{ position: 'absolute', right: width / 3, top: height / 70 }}>
                                <Image
                                    source={require('../assets/images/CLI3/error.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 40, width: height / 40, alignSelf: 'center' }}
                                />
                            </TouchableOpacity>
                        </View>
                        <FloatingLabelInput
                            label="First Name"
                            value={this.state.firstName}
                            width={width / 1.2}
                            returnKeyType={"next"}
                            onSubmitEditing={() => this.customInput2.refs.innerTextInput2.focus()}
                            onChangeText={(text) => this.getFirstName(text)}
                        />
                        <FloatingLabelInput
                            label="Second Name"
                            value={this.state.secondName}
                            width={width / 1.2}
                            returnKeyType={"next"}
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            onSubmitEditing={() => this.customInput3.refs.innerTextInput3.focus()}
                            onChangeText={(text) => this.getSecondName(text)}
                        />
                        <FloatingLabelInput
                            label="Third Name"
                            value={this.state.thirdName}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}

                            onChangeText={(text) => this.getThirdName(text)}
                        />
                        <FloatingLabelInput
                            label="Last Name"
                            value={this.state.LastName}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput4 = ref}
                            refInner="innerTextInput4"
                            onChangeText={(text) => this.getLastName(text)}
                        />

                        <View style={{ flexDirection: "row" }}>
                            <FloatingLabelInput
                                label="Date of Birth"
                                ref={ref => this.customInput2 = ref}
                                refInner="innerTextInput2"
                                value={this.state.dateOfBirth}
                                width={width / 1.2}
                                editable={false}
                                onChangeText={(text) => this.getDateOfBirth(text)}
                                style={styles.passwordStyle} />

                            <TouchableOpacity onPress={() => this.renderDatePicker()}>
                                <Image
                                    source={require('../assets/images/CLI3/calendar.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 13, right: width / 13, top: height / 120 }}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.datePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.renderDatePicker}
                            />
                        </View>
                        
                        <View style={{ flexDirection: "row" }}>
                            <FloatingLabelInput
                                label="Expiry Date"
                                ref={ref => this.customInput2 = ref}
                                refInner="innerTextInput2"
                                value={this.state.expiryDate}
                                width={width / 1.2}
                                editable={false}
                                onChangeText={(text) => this.getExpiryDate(text)}
                                style={styles.passwordStyle} />

                            <TouchableOpacity onPress={() => this.renderDatePicker()}>
                                <Image
                                    source={require('../assets/images/CLI3/calendar.png')}
                                    resizeMode={"contain"}
                                    style={{ height: height / 13, width: width / 13, right: width / 13, top: height / 120 }}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.datePickerVisible}
                                onConfirm={this.handleDatePickedExpiry}
                                onCancel={this.renderDatePicker}
                            />
                        </View>
                        <FloatingLabelInput
                            label="Age (Auto)"
                            value={this.state.age}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getAge(text)}
                        />
                        <View style={{width: width/1.2}}>

                        <Dropdown
                            label='Nationality'
                            data={data}
                            style={{width: 44}}
                        />
                        </View>

                        <FloatingLabelInput
                            label="Contact Number"
                            value={this.state.contactNumber}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getContactNumber(text)}
                        />

                        <FloatingLabelInput
                            label="Email id"
                            value={this.state.email}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getEmail(text)}
                        />

                        <View style={{ width: width/1.2}}>
                        <Dropdown
                            label='Academic Qualification'
                            data={data}
                        />

                        <Dropdown
                            label='Is Working?'
                            data={data}
                        />
                        </View>
                        <FloatingLabelInput
                            label="Place of Work"
                            value={this.state.placeOfWork}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getPlaceofWork(text)}
                        />
                        <FloatingLabelInput
                            label="Designation"
                            value={this.state.designation}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getDesignation(text)}
                        />
                        <FloatingLabelInput
                            label="Monthly Salary"
                            value={this.state.monthlySalary}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            // onChangeText={(text) => this.getThirdName(text)}
                        />
                        {/* <FloatingLabelInput
                            label="Facebook"
                            value={this.state.facebook}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getFacebook(text)}
                        />
                        <FloatingLabelInput
                            label="LinkedIn"
                            value={this.state.linkedIn}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getLinkedIn(text)}
                        />
                        <FloatingLabelInput
                            label="Twitter"
                            value={this.state.twitter}
                            returnKeyType={"next"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getTwitter(text)}
                        />
                        <FloatingLabelInput
                            label="Google Plus"
                            value={this.state.googleplus}
                            returnKeyType={"done"}
                            autoFocus={false}
                            width={width / 1.2}
                            ref={ref => this.customInput3 = ref}
                            refInner="innerTextInput3"
                            // onSubmitEditing={() => this.customInput4.refs.innerTextInput4.focus()}
                            onChangeText={(text) => this.getGooglePlus                                                                                                                                     (text)}
                        /> */}

                        <LinearGradient
                            colors={['#8D218B', '#DA215B', '#F0522D']}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            style={{ height: height / 17, width: width / 2.33, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: 70 }}>
                            <TouchableOpacity style={{ height: height / 18, width: width / 2.38, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('ANGMStepIV')}>
                                <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163" }}>NEXT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </ScrollView>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: width/12,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});
