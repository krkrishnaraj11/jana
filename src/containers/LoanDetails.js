import React, { Component } from 'react';
import { Dimensions, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Header, Card } from 'native-base';
import StepProgressLoan from '../components/StepProgressLoan';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import PercentageCircle from 'react-native-percentage-circle';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FloatingLabelInput from '../components/FloatingLabelInput';
var { height, width } = Dimensions.get('window');
import StepProgress from '../components/StepProgess';



export default class LoanDetails extends Component {
    constructor(){
        super();
        this.state = {
            selectedTab: 'documents'
        }
    }

    renderApplication(){
        return(
            <View>
                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/13, width: height/13}}/>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontFamily: 'Lato-Bold', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>Basic Details</Text>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/50, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>View/Edit Loan, Client or Bank Details</Text>
                    </View>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/13, width: height/13}}/>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontFamily: 'Lato-Bold', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>Personal Details</Text>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/50, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>View/Edit Personal Details</Text>
                    </View>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/13, width: height/13}}/>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontFamily: 'Lato-Bold', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>Project Details</Text>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/50, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>24 Oct 2018, 05:05 PM</Text>
                    </View>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

            </View>
        )
    }

    renderDocuments(){
        let documentData = [{
            value: 'ID',
        }, {
            value: 'Salary Verification',
        }, {
            value: 'Bank Account Card',
        }];
        return(
            <View style={{alignItems: 'center', marginTop: height/25}}>
                <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 45, color: 'rgba(0,0,0,0.54)'}}>DOCUMENTS</Text>
                <ScrollView showsVerticalScrollIndicator={false} > 
                <View style={{width: width/1.2, alignSelf:'center'}}>
                    <Dropdown
                        label={"Select Uploaded Documents"}
                        data ={documentData}
                    />
                </View>
                <LinearGradient
                    colors={['#8D218B', '#DA215B', '#F0522D']}
                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                    style={{ height: height / 17, width: width / 2.1, alignItems: 'center', justifyContent: 'center', alignSelf: "center", marginTop: height / 30, borderRadius: 20, marginBottom: height/40 }}>
                    <TouchableOpacity style={{ height: height / 18, width: width / 2.13, backgroundColor: "#fff", borderRadius: 20, justifyContent: "center", alignItems: "center" }} onPress={() => this.props.navigation.navigate('CLIStepIII')}>
                        <Text style={{ fontFamily: "Lato-Bold", fontSize: 11, color: "#D72163"}}>ATTACH DOCUMENTS</Text>
                    </TouchableOpacity>
                </LinearGradient>

                <Text style={{ fontFamily: 'Lato-Bold', fontSize: height / 50, color: 'rgba(0,0,0,0.54)', alignSelf:'center'}}>DOCUMENTS</Text>
                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/15, width: height/15}}/>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center'}}>ID</Text>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/15, width: height/15}}/>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center'}}>Salary Verification</Text>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', justifyContent:'space-evenly', marginTop: height/30, justifyContent: 'space-between', width: width}}>
                    <Image source={require('../assets/images/CLI10/file-attach.png')} style={{ height: height/15, width: height/15}}/>
                    <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center'}}>Bank Account Card</Text>
                    <Image source={require('../assets/images/CLI10/more.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />
                </ScrollView>
            </View>
        )
    }

    renderActivity(){
        return(
            <View>
                <View style={{flexDirection: 'row', marginTop: height/30, justifyContent: 'space-evenly', width: width}}>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>Forward to Finance Department</Text>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/50, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>Awaiting Decision</Text>
                    </View>
                    <Image source={require('../assets/images/select-loan/warn.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', marginTop: height/30, justifyContent: 'space-evenly', width: width}}>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>Approved by Operation Manager </Text>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/50, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>24 Oct 2018, 05:05 PM</Text>
                    </View>
                    <Image source={require('../assets/images/CLI5/check-circle.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />

                <View style={{flexDirection: 'row', marginTop: height/30, justifyContent: 'space-evenly', width: width}}>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/40, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>Approved by Credit Department</Text>
                        <Text style={{fontFamily: 'Lato-Regular', fontSize: height/50, textAlign:'left', width: width/1.5, alignSelf:'center', color: 'rgba(0,0,0,0.54)'}}>24 Oct 2018, 05:05 PM</Text>
                    </View>
                    <Image source={require('../assets/images/CLI5/check-circle.png')} style={{ height: height/23, width: height/23, alignSelf: 'center'}}/>
                </View>
                <View style={{ height: 1, width: width / 1.1, backgroundColor: 'rgba(0,0,0,0.06)', alignSelf: 'flex-end', marginTop: height / 50}} />
            </View>
        )
    }


    render(){

        return(
            <View style={styles.container}>
                <Header>
                    <LinearGradient
                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#8D218B', '#DA215B', '#F0522D']}
                        style={styles.gradientStyle}
                    >
                       <TouchableOpacity style={{justifyContent:'center'}} onPress={()=> this.props.navigation.goBack()}>
                            <Image source={require('../assets/images/select-loan/back.png')} style={styles.backIconStyle} />
                        </TouchableOpacity>
                        <Text style={styles.homeTitle}>Create New Loan</Text>
                    </LinearGradient>
                </Header>
                <View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: height/40, height: height/10, backgroundColor: '#FFFFFF',shadowColor: '#000',shadowOffset: { width: 0, height: 1 }}}>
                        <View style={{height:height/12, width:height/12, marginLeft: width/20, marginRight:width/20}}>
                                <PercentageCircle radius={27} percent={90} color={"#8D218B"}/>
                        </View>
                        <View style={{width: width/1.7, marginLeft: width/20}}>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: 
                            height/42, color: 'rgba(0,0,0,0.87)', marginBottom: height/500}}>Loan of Mohammed Saud</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: ' rgba(0,0,0,0.32)',marginBottom: height/500}}>Ref: JANA 23654</Text>
                            <Text style={{fontFamily: 'Lato-Regular', width: width/2,fontSize: height/52, color: 'rgba(0,0,0,0.32)'}}>Created 28 Oct 2018, 05:05 PM</Text>
                        </View>
                            
                        <View style={{width: width/4, marginTop: height/100, alignItems: 'center', marginRight:30, marginLeft:10}}>
                            <StepProgress step={5} total={5}/>
                        </View>
                    </View>
                    <Card style={{backgroundColor: '#ffffff', height: height/10, width: width, marginTop: height/60,flexDirection:'row',paddingLeft:1, paddingRight:1}}>
                        
                        <TouchableOpacity style={{justifyContent:'center', borderBottomColor: (this.state.selectedTab == 'activity') ? '#D72163' : 'transparent', borderBottomWidth: (this.state.selectedTab == 'activity') ? height/250 : 0, width: width/3}}  onPress={() => this.setState({selectedTab: 'activity'})}>
                            <Text style={{fontFamily: 'Lato-Bold', fontSize: height/50, textAlign:'center', alignSelf:'center', color: (this.state.selectedTab == 'activity') ? '#D72163' : '#000'}}>Activity</Text>
                        </TouchableOpacity>
                        
                        <View style={{width: width/200,backgroundColor:'rgba(0,0,0,0.12)', height: height/15, alignSelf:'center' }}/>
                        
                        <TouchableOpacity style={{justifyContent:'center', borderBottomColor: (this.state.selectedTab == 'application') ? '#D72163' : 'transparent', borderBottomWidth: (this.state.selectedTab == 'application') ? height/250 : 0, width: width/3}} onPress={() => this.setState({selectedTab: 'application'})}>
                            <Text style={{fontFamily: 'Lato-Bold', fontSize: height/50, textAlign:'center', alignSelf:'center', color: (this.state.selectedTab == 'application') ? '#D72163' : 'rgba(0,0,0,0.54)'}}>Application</Text>
                        </TouchableOpacity>
                        
                        <View style={{width: width/200,backgroundColor:'rgba(0,0,0,0.12)', height: height/15, alignSelf:'center' }}/>
                        
                        <TouchableOpacity style={{justifyContent:'center', borderBottomColor: (this.state.selectedTab == 'documents') ? '#D72163' : 'transparent', borderBottomWidth: (this.state.selectedTab == 'documents') ? height/250 : 0, width: width/3}} onPress={() => this.setState({selectedTab: 'documents'})}>
                            <Text style={{fontFamily: 'Lato-Bold', fontSize: height/50, textAlign:'center', alignSelf:'center', color: (this.state.selectedTab == 'documents') ? '#D72163' : 'rgba(0,0,0,0.54)'}}>Documents</Text>
                        </TouchableOpacity>

                    </Card>

                    <View style={{width: width, backgroundColor:'#fff'}}>
                        {(this.state.selectedTab == 'activity') ? this.renderActivity() : null}
                        {(this.state.selectedTab == 'application') ? this.renderApplication() : null}
                        {(this.state.selectedTab == 'documents') ? this.renderDocuments() : null}
                    </View>
                </View>
            </View>

        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FCFF',
    },
    gradientStyle: {
        height: 56,
        width: 450,
        flexDirection: 'row'
    },
    backIconStyle: {
        resizeMode: 'contain',
        height: height / 20,
        width: height / 20,
        marginLeft: height / 14,
        alignSelf: 'center'
    },
    homeTitle: {
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lato-Bold',
        color: '#fff',
        textAlign: 'left',
        alignSelf: 'center',
        left: height / 22,
        margin: 10,
    },
    statusTitle: {
        fontFamily: 'Lato-Regular',
        fontSize: 12,
        color: 'rgba(0,0,0,0.32)',
        textAlign: 'left',
        marginTop: height / 30,
        marginLeft: height / 40
    }
});