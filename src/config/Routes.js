import React, { Component } from 'react';
import { View, Dimensions, Image } from 'react-native';
import { Header } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { createBottomTabNavigator, createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import Login from '../containers/LoginContainer';
import Home from '../containers/HomeContainer';
import Loan from '../containers/LoanContainer';
import More from '../containers/MoreContainer';
import Activity from '../containers/ActivityContainer';
import SelectLoan from '../containers/SelectLoanContainer';
import CLIStepII from '../containers/CLIStep2Container';
import CLIStepIII from '../containers/CLIStep3Container';
import CLIStepIV from '../containers/CLIStep4Container';
import CLIStepV from '../containers/CLIStep5Container';
import CLIStepVI from '../containers/CLIStep6Container';
import CLIStepVII from '../containers/CLIStep7Container';
import CLIStepVIII from '../containers/CLIStep8Container';
import CLIStepIX from '../containers/CLIStep9Container';
import CLIStepX from '../containers/CLIStep10Container';
import LoanSuccess from '../containers/LoanSuccess';
import ManageGroupMember from '../containers/ManageGroupMemberContainer';
import ANGMStepI from '../containers/ANGMStep1Container';
import ANGMStepII from '../containers/ANGMStep2Container';
import ANGMStepIII from '../containers/ANGMStep3Container';
import ANGMStepIV from '../containers/ANGMStep4Container';
import ANGMStepV from '../containers/ANGMStep5Container';
import ANGMStepVI from '../containers/ANGMStep6Container';
import ANGMStepVII from '../containers/ANGMStep7Container';
import ANGMStepVIII from '../containers/ANGMStep8Container';
import ANGMemberList from '../containers/ANGMListContainer';
import ProfileScreen from '../containers/MyProfileContainer';
import LoanDetails from '../containers/LoanDetails';
var { height, width } = Dimensions.get('window');

export const HomeContainer = createBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: "Home",
      tabBarOptions: {
        activeTintColor: '#D72163',
        inactiveTintColor: 'rgba(0,0,0,0.54)',
        gesturesEnabled: false
      },
      tabBarIcon: ({ focused, horizontal, tintColor }) => (
        <Image
          resizeMode={'contain'}
          source={focused ? require('../assets/images/router/loan-active.png') : require('../assets/images/router/loan-inactive.png')}
          style={{ height: height / 25, width: height / 25 }}
        />
      )
    }
  },
  Loan: {
    screen: createStackNavigator({
      Loan: {
        screen: Loan,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      SelectLoan: {
        screen: SelectLoan,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepII: {
        screen: CLIStepII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepIII: {
        screen: CLIStepIII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepIV: {
        screen: CLIStepIV,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepV: {
        screen: CLIStepV,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepVI: {
        screen: CLIStepVI,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepVII: {
        screen: CLIStepVII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepVIII: {
        screen: CLIStepVIII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepIX: {
        screen: CLIStepIX,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      CLIStepX: {
        screen: CLIStepX,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      LoanSuccess: {
        screen: LoanSuccess,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ManageGroupMember: {
        screen: ManageGroupMember,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepI: {
        screen: ANGMStepI,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepII: {
        screen: ANGMStepII,
        navigationOptions: {
          header: null
        }
      },
      ANGMStepIII: {
        screen: ANGMStepIII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepIV: {
        screen: ANGMStepIV,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepV: {
        screen: ANGMStepV,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepVI: {
        screen: ANGMStepVI,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepVII: {
        screen: ANGMStepVII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepVIII: {
        screen: ANGMStepVIII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMemberList: {
        screen: ANGMemberList,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      LoanDetails: {
        screen: LoanDetails,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      }
    }),
    navigationOptions: {
      title: "Loan",
      tabBarOptions: {
        activeTintColor: '#D72163',
        inactiveTintColor: 'rgba(0,0,0,0.54)',
        gesturesEnabled: false
      },
      tabBarIcon: ({ focused, horizontal, tintColor }) => (
        <Image
          resizeMode={'contain'}
          source={focused ? require('../assets/images/router/loan-active.png') : require('../assets/images/router/loan-inactive.png')}
          style={{ height: height / 25, width: height / 25 }}
        />
      )
    }
  },
  Activity: {
    screen: Activity,
    navigationOptions: {
      title: "Activity",
      tabBarOptions: {
        activeTintColor: '#D72163',
        inactiveTintColor: 'rgba(0,0,0,0.54)',
        gesturesEnabled: false
      },
      tabBarIcon: ({ focused, horizontal, tintColor }) => (
        <Image
          resizeMode={'contain'}
          source={focused ? require('../assets/images/router/activity-active.png') : require('../assets/images/router/activity-inactive.png')}
          style={{ height: height / 25, width: height / 25 }}
        />
      )
    }
  },
  More: {
    screen: createStackNavigator({
      More: {
        screen: More,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      Profile: {
        screen: ProfileScreen,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      ANGMStepVIII: {
        screen: ANGMStepVIII,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
    }),
    navigationOptions: {
      title: "More",
      tabBarOptions: {
        activeTintColor: '#D72163',
        inactiveTintColor: 'rgba(0,0,0,0.54)',
        gesturesEnabled: false
      },
      tabBarIcon: ({ focused, horizontal, tintColor }) => (
        <Image
          resizeMode={'contain'}
          source={focused ? require('../assets/images/router/more-active.png') : require('../assets/images/router/more-inactive.png')}
          style={{ height: height / 25, width: height / 25 }}
        />
      )
    }
  },
});

const GradientHeader = props => (
  <View style={{ backgroundColor: '#eee' }}>
    <LinearGradient
      start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
      colors={['#8D218B', '#DA215B', '#F0522D']}
      style={[{ height: 300 }]}
    />
    <Header {...props} style={{ backgroundColor: 'transparent' }} />
  </View>
);

const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      LogIn: {
        screen: HomeContainer
      },
      LogOut: {
        screen: Login
      }
    },
    {
      initialRouteName: signedIn ? "LogIn" : "LogOut",
    },
  );
};


export const LoggedIn = createAppContainer(createRootNavigator(false));
export const HomeC = createAppContainer(HomeContainer);