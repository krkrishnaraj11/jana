export const API = {
    login: 'http://3.0.235.247/jana/login',
    logout: 'http://3.0.235.247/jana/logout',
    profile: 'http://3.0.235.247/jana/my_profile',

    activityList: 'http://3.0.235.247/jana/loan_activity_list',
    loanList: 'http://3.0.235.247/jana/loan',

    loanProductList: 'http://3.0.235.247/jana/loan_product',
    nationalityCodeList: 'http://3.0.235.247/jana/NationalityCodes',

    selectLoan: 'http://3.0.235.247/jana/loan_save_step_1',
    CLIStep2: 'http://3.0.235.247/jana/Step02_LoanDetails', 
    CLIStep3: 'http://3.0.235.247/jana/Step03_ClientDetails',

}