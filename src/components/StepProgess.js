import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
var {width,height} = Dimensions.get('window');

export default class StepProgress extends Component {
    render() {
        const {step} = this.props;
        const {total} = this.props;
        
        return(
            <View style={{width: this.props.width, marginTop: height/100, alignItems: 'center'}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{width: width/20, height: width/60, backgroundColor: (step >= 1) ? '#F8BBD0' : '#FAFAFA', borderRadius: 2, marginRight: width/120}}/>

                    <View style={{width: width/20, height: width/60, backgroundColor: (step >= 2)? '#FBC9D9' : '#FAFAFA', borderRadius: 2, marginRight: width/120}}/>

                    <View style={{width: width/20, height: width/60, backgroundColor: (step >= 3)? '#BEE5E2' : '#FAFAFA', borderRadius: 2, marginRight: width/120}}/>

                    <View style={{width: width/20, height: width/60, backgroundColor: (step >= 4)? '#DCEDC8' : '#FAFAFA', borderRadius: 2, marginRight: width/120}}/>

                    <View style={{width: width/20, height: width/60, backgroundColor: (step >= 5)?  '#ABC986' : '#FAFAFA', borderRadius: 2}}/>
            </View>
            <Text style={{textAlign: 'right', marginTop: 5, fontFamily: 'Lato-Regular', color: '#757575', width: width/3.7}}>({step} of {total})</Text>
        </View>
        )
    }
}