import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import { ForceTouchGestureHandler } from 'react-native-gesture-handler';
var {width,height} = Dimensions.get('window');

export default class StepProgressLoan extends Component {
    stepRender(){
        for(i=0;i<9;i++){
            return(
            <View style={{width: width/20, height: width/120, borderRadius: 50, backgroundColor: '#E0E0E0',marginRight: width/50}}/>                   
            )                
        }
    }

    render() {
        var step = this.props.step;
        var total  = this.props.total;
        // console.log(total);
        var myloop = [];
        for (let i = 0; i < 10; i++) {
            myloop.push(
                <View style={{width: width/20, height: width/120, borderRadius: 50, backgroundColor: (i<step) ? '#D72163' : '#E0E0E0',marginRight: width/50}}/>
            );
        }
        return(
            <View style={{alignItems: 'center', height: height/7, justifyContent: 'center'}}>
            <Text>STEP {step} OF {total}</Text>
            <View style={{flexDirection: 'row', marginTop : height/44}}>
            {myloop}
            </View>            
        </View>
        )
    }
}